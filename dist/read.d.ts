import { PrismaClient } from "../prisma-client";
import winston from "winston";
import { Projection, FetchFunction, ReadLastMessageFunction, ReadFunction, ReadCategoryFunction, ReadStreamFunction, SingleStreamMessage, InnerMessage } from "./types";
/**
 * @description Projects an array of events, running them through projection.
 * Does so by checking to see if `projection` defines a handler for each event
 * type.  If it does, then it calls into that function with the running state
 * and current event, passing the return value to the next iteration.
 */
export declare function project<Events extends InnerMessage, Entity>(events: SingleStreamMessage<Events>[], projection: Projection<Events, Entity>): Entity;
export default function createRead({ prisma, logger, }: {
    prisma: PrismaClient;
    logger: winston.Logger;
}): {
    read: ReadFunction;
    readStream: ReadStreamFunction;
    readCategory: ReadCategoryFunction;
    readLastMessage: ReadLastMessageFunction;
    fetch: FetchFunction;
};
