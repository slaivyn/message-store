import { MessageStore } from "./index";

export interface RawMessage {
  id: string;
  stream_name: string;
  type: string;
  position: string;
  global_position: string;
  data: string;
  metadata: string;
  time: Date;
}

/**
 * From https://github.com/sindresorhus/type-fest/
 * Matches a JSON object.
 * This type can be useful to enforce some input to be JSON-compatible or as a super-type to be extended from.
 */
export type JsonObject = { [Key in string]?: JsonValue };

/**
 * From https://github.com/sindresorhus/type-fest/
 * Matches a JSON array.
 */
export interface JsonArray extends Array<JsonValue> {}

/**
 * From https://github.com/sindresorhus/type-fest/
 * Matches any valid JSON value.
 */
export type JsonValue =
  | string
  | number
  | boolean
  | null
  | JsonObject
  | JsonArray;

type Data = JsonObject;
export type DefaultMetadata = JsonObject & {
  traceId?: string;
  originStreamName?: string;
};

export interface InnerMessage<
  T extends any = any,
  D extends Data | null = Data,
  MD extends DefaultMetadata = DefaultMetadata
> {
  type: T;
  data: D;
  metadata: MD | undefined | null;
}

export interface StreamMessage<T extends InnerMessage = InnerMessage> {
  id: string;
  time: Date;
  globalPosition: bigint;
  type: T["type"];
  data: T["data"];
  metadata: T["metadata"];
  streamName?: string;
  position?: bigint;
  streams?: {
    streamName: string;
    position: bigint;
  }[];
}
export type SingleStreamMessage<T extends InnerMessage = InnerMessage> = Omit<
  StreamMessage<T>,
  "streams"
> & {
  streamName: string;
  position: bigint;
};
export type MultiStreamMessage<T extends InnerMessage = InnerMessage> = Omit<
  StreamMessage<T>,
  "streamName" | "position"
> & {
  streams: {
    streamName: string;
    position: bigint;
  }[];
};
export type Event = StreamMessage;
export type Command = MultiStreamMessage;

type BaseEntity = { [key: string]: any };

export type Projection<
  Events extends InnerMessage,
  Entity extends BaseEntity = BaseEntity
> = {
  [K in Events as K["type"]]?: (
    entity: Entity,
    event: SingleStreamMessage<K>
  ) => Entity;
} & { $init: () => Entity };

type AdditionnalFields = {
  [key: string]: any;
};

export type CommandHandlersFactory<
  Events extends InnerMessage = InnerMessage,
  I extends AdditionnalFields = AdditionnalFields
> = ({
  messageStore,
}: I & { messageStore: MessageStore }) => MultiStreamMessageHandlers<Events>;

export type SingleCommandHandlerFactory<
  Events extends InnerMessage = InnerMessage,
  I extends AdditionnalFields = AdditionnalFields
> = ({
  messageStore,
}: I & { messageStore: MessageStore }) => MultiStreamMessageHandler<Events>;

export type WriteMessageFunctionArg = [
  string,
  [string, bigint | null][],
  string,
  Data,
  DefaultMetadata | undefined | null
];

interface IMessage<T extends InnerMessage = InnerMessage> {
  id?: StreamMessage["id"];
  type: T["type"];
  data: T["data"];
  metadata?: T["metadata"];
}

export type StreamMessageHandler<T extends InnerMessage = InnerMessage> = (
  message: StreamMessage<T>
) => Promise<any>;
export type StreamMessageHandlers<T extends InnerMessage = InnerMessage> = {
  [K in T as K["type"]]?: StreamMessageHandler<K>;
};

export type SingleStreamMessageHandler<T extends InnerMessage = InnerMessage> =
  (message: SingleStreamMessage<T>) => Promise<any>;

export type SingleStreamMessageHandlers<T extends InnerMessage = InnerMessage> =
  {
    [K in T as K["type"]]?: SingleStreamMessageHandler<K>;
  };

export type MultiStreamMessageHandler<
  Event extends InnerMessage = InnerMessage
> = (message: MultiStreamMessage<Event>) => Promise<any>;

export type MultiStreamMessageHandlers<
  Events extends InnerMessage = InnerMessage
> = {
  // [K in Events]: MultiStreamMessageHandler<K>;
  [K in Events as K["type"]]?: MultiStreamMessageHandler<K>;
};

export type AggregatorHandlersFactory<
  Events extends InnerMessage,
  I extends AdditionnalFields = AdditionnalFields
> = ({
  messageStore,
  queries,
}: I & {
  messageStore: MessageStore;
}) => MultiStreamMessageHandlers<Events>;

export type ReadFunction = (
  streamName: string,
  fromPosition?: bigint,
  maxMessages?: number
) => Promise<SingleStreamMessage[] | MultiStreamMessage[]>;

export type ReadCategoryFunction = (
  streamName: string,
  fromPosition?: bigint,
  maxMessages?: number
) => Promise<MultiStreamMessage[]>;

export type ReadStreamFunction = <Events extends InnerMessage>(
  streamName: string,
  fromPosition?: bigint,
  maxMessages?: number
) => Promise<SingleStreamMessage<Events>[]>;

export type ReadLastMessageFunction = <Event extends InnerMessage>(
  streamName: string,
  fromPosition?: bigint,
  maxMessages?: number
) => Promise<SingleStreamMessage<Event> | null>;

export type FetchFunction = <
  Events extends InnerMessage,
  Entity extends BaseEntity = BaseEntity
>(
  streamName: string,
  projection: Projection<Events, Entity>
) => Promise<Entity>;

type Stream = string | { name: string; expectedVersion?: bigint };

export type WriteFunction = <T extends InnerMessage>(
  stream: Stream | Stream[],
  message: IMessage<T>
) => Promise<{ rows: number[] }>;

interface ICreateSubscription<Events extends InnerMessage> {
  streamName: string;
  handlers: StreamMessageHandlers<Events>;
  // & { $any?: StreamMessageHandler<InnerMessage>}
  subscriberId: string;
  messagesPerTick?: number;
  positionUpdateInterval?: number;
  originStreamName?: string | null;
  tickIntervalMs?: number;
}

export type CreateSubscriptionFunction = <Events extends InnerMessage>(
  arg: ICreateSubscription<Events>
) => {
  loadPosition: () => Promise<void>;
  start: () => Promise<void>;
  stop: () => void;
  writePosition: (position: bigint) => Promise<unknown>;
};

// export type MessageStore = {
//   write: WriteFunction;
//   createSubscription: CreateSubscriptionFunction;
//   read: ReadFunction;
//   readStream: ReadStreamFunction;
//   readCategory: ReadCategoryFunction;
//   readLastMessage: ReadLastMessageFunction;
//   fetch: FetchFunction;
//   stop: () => Promise<unknown>;
// };
