"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const json_to_object_1 = __importDefault(require("../helpers/json-to-object"));
function getStreamMessages(prisma, streamName, fromPosition = BigInt(0), maxMessages = 1000) {
    if (!streamName.includes("-"))
        throw `Must be a stream name: ${streamName}`;
    return prisma.messageStream
        .findMany({
        where: {
            streamName,
            position: { gte: fromPosition },
        },
        take: maxMessages,
        orderBy: { position: "asc" },
        include: {
            message: true,
        },
    })
        .then((messages) => messages.map(({ streamName, position, message }) => (Object.assign(Object.assign({}, message), { data: json_to_object_1.default(message.data), metadata: json_to_object_1.default(message.metadata), streamName,
        position }))));
}
exports.default = getStreamMessages;
