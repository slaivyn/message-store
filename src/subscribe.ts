import Bluebird from "bluebird";
import { v4 as uuid } from "uuid";
import winston from "winston";

import category from "./category";
import {
  CreateSubscriptionFunction,
  InnerMessage,
  MultiStreamMessage,
  ReadCategoryFunction,
  ReadLastMessageFunction,
  StreamMessage,
  WriteFunction,
} from "./types";

interface IConfigureCreateSubscription {
  readCategory: ReadCategoryFunction;
  readLastMessage: ReadLastMessageFunction;
  write: WriteFunction;
  logger: winston.Logger;
}

export default function configureCreateSubscription({
  readCategory,
  readLastMessage,
  write,
  logger,
}: IConfigureCreateSubscription) {
  const createSubscription: CreateSubscriptionFunction = ({
    streamName,
    handlers,
    subscriberId,
    messagesPerTick = 100,
    positionUpdateInterval = 100,
    originStreamName = null,
    tickIntervalMs = 100,
  }) => {
    const subscriberStreamName = `subscriberPosition-${subscriberId}`;

    let currentPosition = BigInt(0);
    let messagesSinceLastPositionWrite = 0;
    let keepGoing = true;

    function writePosition(position: bigint) {
      const positionEvent = {
        id: uuid(),
        type: "Read",
        data: { position: position.toString() },
      };

      return write(subscriberStreamName, positionEvent);
    }

    function updateReadPosition(position: bigint): Promise<unknown> {
      currentPosition = position;
      messagesSinceLastPositionWrite += 1;

      if (messagesSinceLastPositionWrite === positionUpdateInterval) {
        messagesSinceLastPositionWrite = 0;

        return writePosition(position);
      }

      return Bluebird.resolve();
    }

    function loadPosition() {
      return readLastMessage(subscriberStreamName).then((message) => {
        currentPosition = BigInt(
          message?.data && typeof message.data === "object"
            ? (message.data as { position: string }).position
            : 0
        );
      });
    }

    function filterOnOriginMatch<T extends InnerMessage>(
      messages: MultiStreamMessage<T>[]
    ) {
      if (!originStreamName) {
        return messages;
      }

      return messages.filter((message) => {
        if (!message.metadata || typeof message.metadata !== "object")
          return false;
        const originCategory = category(message.metadata.originStreamName);

        return originStreamName === originCategory;
      });
    }

    function getNextBatchOfMessages() {
      return readCategory(
        streamName,
        currentPosition + BigInt(1),
        messagesPerTick
      ).then(filterOnOriginMatch);
    }

    function handleMessage<Events extends InnerMessage>(
      message: StreamMessage<Events>
    ): Promise<unknown> {
      const handler = handlers[message.type]; //|| handlers.$any;

      return handler ? handler(message) : Promise.resolve(true);
    }

    function processBatch<T extends InnerMessage>(
      messages: StreamMessage<T>[]
    ) {
      return Bluebird.each(messages, (message) =>
        handleMessage(message)
          .then(() => updateReadPosition(message.globalPosition))
          .catch((err) => {
            logError(message, err);

            // Re-throw so that we can break the chain
            throw err;
          })
      ).then(() => messages.length);
    }

    function logError(lastMessage: StreamMessage, error: any) {
      // eslint-disable-next-line no-console
      logger.error(
        `error processing message (${subscriberId}-${lastMessage.id}): ${error}`
      );
    }

    /**
     * @description - Generally not called from the outside.  This function is
     *   called on each of the timeouts to see if there are new events that need
     *   processing.
     */
    function tick() {
      return getNextBatchOfMessages()
        .then(processBatch)
        .catch((err) => {
          // eslint-disable-next-line no-console
          logger.error("Error processing batch", err);

          stop();
        });
    }

    async function poll() {
      await loadPosition();

      // eslint-disable-next-line no-unmodified-loop-condition
      while (keepGoing) {
        const messagesProcessed = await tick();

        if (messagesProcessed === 0) {
          await Bluebird.delay(tickIntervalMs);
        }
      }
    }

    function start() {
      // eslint-disable-next-line
      console.log(`Started ${subscriberId}`);

      return poll();
    }

    function stop() {
      // eslint-disable-next-line
      console.log(`Stopped ${subscriberId}`);

      keepGoing = false;
    }

    return {
      loadPosition,
      start,
      stop,
      // tick,
      writePosition,
    };
  };
  return createSubscription;
}
