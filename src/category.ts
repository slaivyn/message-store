function category(streamName: string | null | undefined) {
  if (streamName == null) {
    return "";
  }

  return streamName.split("-")[0];
}

export default category;
