import { PrismaClient } from "../../prisma-client";
import { MultiStreamMessage } from "../types";
declare function getCategoryMessages(prisma: PrismaClient, streamName: string, fromPosition?: bigint, maxMessages?: number): Promise<MultiStreamMessage[]>;
export default getCategoryMessages;
