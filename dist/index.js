"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.project = exports.VersionConflictError = void 0;
const prisma_client_1 = require("../prisma-client");
const read_1 = __importDefault(require("./read"));
const subscribe_1 = __importDefault(require("./subscribe"));
const write_1 = __importDefault(require("./write"));
const winston_1 = __importDefault(require("winston"));
var version_conflict_error_1 = require("./version-conflict-error");
Object.defineProperty(exports, "VersionConflictError", { enumerable: true, get: function () { return __importDefault(version_conflict_error_1).default; } });
var read_2 = require("./read");
Object.defineProperty(exports, "project", { enumerable: true, get: function () { return read_2.project; } });
function createMessageStore({ datasource, logger: providedLogger, }) {
    const logger = providedLogger ||
        winston_1.default.createLogger({
            transports: [
                new winston_1.default.transports.Console({
                    level: "info",
                    format: winston_1.default.format.combine(winston_1.default.format.colorize(), winston_1.default.format.simple()),
                }),
            ],
        });
    const prisma = new prisma_client_1.PrismaClient({
        datasources: { db: datasource },
        __internal: {
            useUds: true,
        },
    });
    const stop = () => {
        console.log("stop message-store prisma");
        return prisma.$disconnect();
    };
    prisma.$on("beforeExit", () => {
        console.log("beforeExit");
    });
    const write = write_1.default({ prisma, logger });
    const { read, readStream, readCategory, readLastMessage, fetch } = read_1.default({ prisma, logger });
    const createSubscription = subscribe_1.default({
        readCategory,
        readLastMessage,
        write,
        logger,
    });
    return {
        prisma,
        write,
        createSubscription,
        read,
        readStream,
        readCategory,
        readLastMessage,
        fetch,
        stop,
    };
}
exports.default = createMessageStore;
