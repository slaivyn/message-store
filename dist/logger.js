"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const createLogger = ({ Sentry }) => {
    const error = (exception) => {
        if (Sentry) {
            Sentry.captureException(exception);
        }
        console.error(exception);
    };
    return {
        error,
    };
};
exports.default = createLogger;
