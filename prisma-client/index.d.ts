
/**
 * Client
**/

import * as runtime from '@prisma/client/runtime';
declare const prisma: unique symbol
export type PrismaPromise<A> = Promise<A> & {[prisma]: true}
type UnwrapPromise<P extends any> = P extends Promise<infer R> ? R : P
type UnwrapTuple<Tuple extends readonly unknown[]> = {
  [K in keyof Tuple]: K extends `${number}` ? Tuple[K] extends PrismaPromise<infer X> ? X : UnwrapPromise<Tuple[K]> : UnwrapPromise<Tuple[K]>
};


/**
 * Model Message
 */

export type Message = {
  id: string
  type: string
  globalPosition: bigint
  data: Prisma.JsonValue | null
  metadata: Prisma.JsonValue | null
  time: Date
}

/**
 * Model MessageStream
 */

export type MessageStream = {
  messageId: string
  streamName: string
  position: bigint
}


/**
 * ##  Prisma Client ʲˢ
 * 
 * Type-safe database client for TypeScript & Node.js (ORM replacement)
 * @example
 * ```
 * const prisma = new PrismaClient()
 * // Fetch zero or more Messages
 * const messages = await prisma.message.findMany()
 * ```
 *
 * 
 * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client).
 */
export class PrismaClient<
  T extends Prisma.PrismaClientOptions = Prisma.PrismaClientOptions,
  U = 'log' extends keyof T ? T['log'] extends Array<Prisma.LogLevel | Prisma.LogDefinition> ? Prisma.GetEvents<T['log']> : never : never,
  GlobalReject = 'rejectOnNotFound' extends keyof T
    ? T['rejectOnNotFound']
    : false
      > {
      /**
       * @private
       */
      private fetcher;
      /**
       * @private
       */
      private readonly dmmf;
      /**
       * @private
       */
      private connectionPromise?;
      /**
       * @private
       */
      private disconnectionPromise?;
      /**
       * @private
       */
      private readonly engineConfig;
      /**
       * @private
       */
      private readonly measurePerformance;

    /**
   * ##  Prisma Client ʲˢ
   * 
   * Type-safe database client for TypeScript & Node.js (ORM replacement)
   * @example
   * ```
   * const prisma = new PrismaClient()
   * // Fetch zero or more Messages
   * const messages = await prisma.message.findMany()
   * ```
   *
   * 
   * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client).
   */

  constructor(optionsArg ?: Prisma.Subset<T, Prisma.PrismaClientOptions>);
  $on<V extends (U | 'beforeExit')>(eventType: V, callback: (event: V extends 'query' ? Prisma.QueryEvent : V extends 'beforeExit' ? () => Promise<void> : Prisma.LogEvent) => void): void;

  /**
   * Connect with the database
   */
  $connect(): Promise<void>;

  /**
   * Disconnect from the database
   */
  $disconnect(): Promise<any>;

  /**
   * Add a middleware
   */
  $use(cb: Prisma.Middleware): void

  /**
   * Executes a raw query and returns the number of affected rows
   * @example
   * ```
   * // With parameters use prisma.executeRaw``, values will be escaped automatically
   * const result = await prisma.executeRaw`UPDATE User SET cool = ${true} WHERE id = ${1};`
   * // Or
   * const result = await prisma.executeRaw('UPDATE User SET cool = $1 WHERE id = $2 ;', true, 1)
  * ```
  * 
  * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/raw-database-access).
  */
  $executeRaw < T = any > (query: string | TemplateStringsArray | Prisma.Sql, ...values: any[]): PrismaPromise<number>;

  /**
   * Performs a raw query and returns the SELECT data
   * @example
   * ```
   * // With parameters use prisma.queryRaw``, values will be escaped automatically
   * const result = await prisma.queryRaw`SELECT * FROM User WHERE id = ${1} OR email = ${'ema.il'};`
   * // Or
   * const result = await prisma.queryRaw('SELECT * FROM User WHERE id = $1 OR email = $2;', 1, 'ema.il')
  * ```
  * 
  * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/raw-database-access).
  */
  $queryRaw < T = any > (query: string | TemplateStringsArray | Prisma.Sql, ...values: any[]): PrismaPromise<T>;

  /**
   * Allows the running of a sequence of read/write operations that are guaranteed to either succeed or fail as a whole.
   * @example
   * ```
   * const [george, bob, alice] = await prisma.transaction([
   *   prisma.user.create({ data: { name: 'George' } }),
   *   prisma.user.create({ data: { name: 'Bob' } }),
   *   prisma.user.create({ data: { name: 'Alice' } }),
   * ])
   * ```
   * 
   * Read more in our [docs](https://www.prisma.io/docs/concepts/components/prisma-client/transactions).
   */
  $transaction<P extends PrismaPromise<any>[]>(arg: [...P]): Promise<UnwrapTuple<P>>

      /**
   * `prisma.message`: Exposes CRUD operations for the **Message** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Messages
    * const messages = await prisma.message.findMany()
    * ```
    */
  get message(): Prisma.MessageDelegate<GlobalReject>;

  /**
   * `prisma.messageStream`: Exposes CRUD operations for the **MessageStream** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more MessageStreams
    * const messageStreams = await prisma.messageStream.findMany()
    * ```
    */
  get messageStream(): Prisma.MessageStreamDelegate<GlobalReject>;
}

export namespace Prisma {
  export import DMMF = runtime.DMMF

  /**
   * Prisma Errors
   */
  export import PrismaClientKnownRequestError = runtime.PrismaClientKnownRequestError
  export import PrismaClientUnknownRequestError = runtime.PrismaClientUnknownRequestError
  export import PrismaClientRustPanicError = runtime.PrismaClientRustPanicError
  export import PrismaClientInitializationError = runtime.PrismaClientInitializationError
  export import PrismaClientValidationError = runtime.PrismaClientValidationError

  /**
   * Re-export of sql-template-tag
   */
  export import sql = runtime.sqltag
  export import empty = runtime.empty
  export import join = runtime.join
  export import raw = runtime.raw
  export import Sql = runtime.Sql

  /**
   * Decimal.js
   */
  export import Decimal = runtime.Decimal

  /**
   * Prisma Client JS version: 2.23.0
   * Query Engine version: adf5e8cba3daf12d456d911d72b6e9418681b28b
   */
  export type PrismaVersion = {
    client: string
  }

  export const prismaVersion: PrismaVersion 

  /**
   * Utility Types
   */

  /**
   * From https://github.com/sindresorhus/type-fest/
   * Matches a JSON object.
   * This type can be useful to enforce some input to be JSON-compatible or as a super-type to be extended from. 
   */
  export type JsonObject = {[Key in string]?: JsonValue}
 
  /**
   * From https://github.com/sindresorhus/type-fest/
   * Matches a JSON array.
   */
  export interface JsonArray extends Array<JsonValue> {}
 
  /**
   * From https://github.com/sindresorhus/type-fest/
   * Matches any valid JSON value.
   */
  export type JsonValue = string | number | boolean | null | JsonObject | JsonArray

  /**
   * Same as JsonObject, but allows undefined
   */
  export type InputJsonObject = {[Key in string]?: JsonValue}
 
  export interface InputJsonArray extends Array<JsonValue> {}
 
  export type InputJsonValue = undefined |  string | number | boolean | null | InputJsonObject | InputJsonArray
   type SelectAndInclude = {
    select: any
    include: any
  }
  type HasSelect = {
    select: any
  }
  type HasInclude = {
    include: any
  }
  type CheckSelect<T, S, U> = T extends SelectAndInclude
    ? 'Please either choose `select` or `include`'
    : T extends HasSelect
    ? U
    : T extends HasInclude
    ? U
    : S

  /**
   * Get the type of the value, that the Promise holds.
   */
  export type PromiseType<T extends PromiseLike<any>> = T extends PromiseLike<infer U> ? U : T;

  /**
   * Get the return type of a function which returns a Promise.
   */
  export type PromiseReturnType<T extends (...args: any) => Promise<any>> = PromiseType<ReturnType<T>>

  /**
   * From T, pick a set of properties whose keys are in the union K
   */
  type Prisma__Pick<T, K extends keyof T> = {
      [P in K]: T[P];
  };


  export type Enumerable<T> = T | Array<T>;

  export type RequiredKeys<T> = {
    [K in keyof T]-?: {} extends Prisma__Pick<T, K> ? never : K
  }[keyof T]

  export type TruthyKeys<T> = {
    [key in keyof T]: T[key] extends false | undefined | null ? never : key
  }[keyof T]

  export type TrueKeys<T> = TruthyKeys<Prisma__Pick<T, RequiredKeys<T>>>

  /**
   * Subset
   * @desc From `T` pick properties that exist in `U`. Simple version of Intersection
   */
  export type Subset<T, U> = {
    [key in keyof T]: key extends keyof U ? T[key] : never;
  };

  /**
   * SelectSubset
   * @desc From `T` pick properties that exist in `U`. Simple version of Intersection.
   * Additionally, it validates, if both select and include are present. If the case, it errors.
   */
  export type SelectSubset<T, U> = {
    [key in keyof T]: key extends keyof U ? T[key] : never
  } &
    (T extends SelectAndInclude
      ? 'Please either choose `select` or `include`.'
      : {})

  /**
   * Subset + Intersection
   * @desc From `T` pick properties that exist in `U` and intersect `K`
   */
  export type SubsetIntersection<T, U, K> = {
    [key in keyof T]: key extends keyof U ? T[key] : never
  } &
    K

  type Without<T, U> = { [P in Exclude<keyof T, keyof U>]?: never };

  /**
   * XOR is needed to have a real mutually exclusive union type
   * https://stackoverflow.com/questions/42123407/does-typescript-support-mutually-exclusive-types
   */
  type XOR<T, U> = (T | U) extends object ? (Without<T, U> & U) | (Without<U, T> & T) : T | U;


  /**
   * Is T a Record?
   */
  type IsObject<T extends any> = T extends Array<any>
  ? False
  : T extends Date
  ? False
  : T extends Buffer
  ? False
  : T extends BigInt
  ? False
  : T extends object
  ? True
  : False


  /**
   * If it's T[], return T
   */
  export type UnEnumerate<T extends unknown> = T extends Array<infer U> ? U : T

  /**
   * From ts-toolbelt
   */

  type __Either<O extends object, K extends Key> = Omit<O, K> &
    {
      // Merge all but K
      [P in K]: Prisma__Pick<O, P & keyof O> // With K possibilities
    }[K]

  type EitherStrict<O extends object, K extends Key> = Strict<__Either<O, K>>

  type EitherLoose<O extends object, K extends Key> = ComputeRaw<__Either<O, K>>

  type _Either<
    O extends object,
    K extends Key,
    strict extends Boolean
  > = {
    1: EitherStrict<O, K>
    0: EitherLoose<O, K>
  }[strict]

  type Either<
    O extends object,
    K extends Key,
    strict extends Boolean = 1
  > = O extends unknown ? _Either<O, K, strict> : never

  export type Union = any

  type PatchUndefined<O extends object, O1 extends object> = {
    [K in keyof O]: O[K] extends undefined ? At<O1, K> : O[K]
  } & {}

  /** Helper Types for "Merge" **/
  export type IntersectOf<U extends Union> = (
    U extends unknown ? (k: U) => void : never
  ) extends (k: infer I) => void
    ? I
    : never

  export type Overwrite<O extends object, O1 extends object> = {
      [K in keyof O]: K extends keyof O1 ? O1[K] : O[K];
  } & {};

  type _Merge<U extends object> = IntersectOf<Overwrite<U, {
      [K in keyof U]-?: At<U, K>;
  }>>;

  type Key = string | number | symbol;
  type AtBasic<O extends object, K extends Key> = K extends keyof O ? O[K] : never;
  type AtStrict<O extends object, K extends Key> = O[K & keyof O];
  type AtLoose<O extends object, K extends Key> = O extends unknown ? AtStrict<O, K> : never;
  export type At<O extends object, K extends Key, strict extends Boolean = 1> = {
      1: AtStrict<O, K>;
      0: AtLoose<O, K>;
  }[strict];

  export type ComputeRaw<A extends any> = A extends Function ? A : {
    [K in keyof A]: A[K];
  } & {};

  export type OptionalFlat<O> = {
    [K in keyof O]?: O[K];
  } & {};

  type _Record<K extends keyof any, T> = {
    [P in K]: T;
  };

  type _Strict<U, _U = U> = U extends unknown ? U & OptionalFlat<_Record<Exclude<Keys<_U>, keyof U>, never>> : never;

  export type Strict<U extends object> = ComputeRaw<_Strict<U>>;
  /** End Helper Types for "Merge" **/

  export type Merge<U extends object> = ComputeRaw<_Merge<Strict<U>>>;

  /**
  A [[Boolean]]
  */
  export type Boolean = True | False

  // /**
  // 1
  // */
  export type True = 1

  /**
  0
  */
  export type False = 0

  export type Not<B extends Boolean> = {
    0: 1
    1: 0
  }[B]

  export type Extends<A1 extends any, A2 extends any> = [A1] extends [never]
    ? 0 // anything `never` is false
    : A1 extends A2
    ? 1
    : 0

  export type Has<U extends Union, U1 extends Union> = Not<
    Extends<Exclude<U1, U>, U1>
  >

  export type Or<B1 extends Boolean, B2 extends Boolean> = {
    0: {
      0: 0
      1: 1
    }
    1: {
      0: 1
      1: 1
    }
  }[B1][B2]

  export type Keys<U extends Union> = U extends unknown ? keyof U : never

  type Exact<A, W = unknown> = 
  W extends unknown ? A extends Narrowable ? Cast<A, W> : Cast<
  {[K in keyof A]: K extends keyof W ? Exact<A[K], W[K]> : never},
  {[K in keyof W]: K extends keyof A ? Exact<A[K], W[K]> : W[K]}>
  : never;

  type Narrowable = string | number | boolean | bigint;

  type Cast<A, B> = A extends B ? A : B;

  export const type: unique symbol;

  export function validator<V>(): <S>(select: Exact<S, V>) => S;

  /**
   * Used by group by
   */

  export type GetScalarType<T, O> = O extends object ? {
    [P in keyof T]: P extends keyof O
      ? O[P]
      : never
  } : never

  type FieldPaths<
    T,
    U = Omit<T, '_avg' | '_sum' | '_count' | '_min' | '_max'>
  > = IsObject<T> extends True ? U : T

  type GetHavingFields<T> = {
    [K in keyof T]: Or<
      Or<Extends<'OR', K>, Extends<'AND', K>>,
      Extends<'NOT', K>
    > extends True
      ? // infer is only needed to not hit TS limit
        // based on the brilliant idea of Pierre-Antoine Mills
        // https://github.com/microsoft/TypeScript/issues/30188#issuecomment-478938437
        T[K] extends infer TK
        ? GetHavingFields<UnEnumerate<TK> extends object ? Merge<UnEnumerate<TK>> : never>
        : never
      : {} extends FieldPaths<T[K]>
      ? never
      : K
  }[keyof T]

  /**
   * Convert tuple to union
   */
  type _TupleToUnion<T> = T extends (infer E)[] ? E : never
  type TupleToUnion<K extends readonly any[]> = _TupleToUnion<K>
  type MaybeTupleToUnion<T> = T extends any[] ? TupleToUnion<T> : T

  /**
   * Like `Pick`, but with an array
   */
  type PickArray<T, K extends Array<keyof T>> = Prisma__Pick<T, TupleToUnion<K>>

  /**
   * Exclude all keys with underscores
   */
  type ExcludeUnderscoreKeys<T extends string> = T extends `_${string}` ? never : T

  class PrismaClientFetcher {
    private readonly prisma;
    private readonly debug;
    private readonly hooks?;
    constructor(prisma: PrismaClient<any, any>, debug?: boolean, hooks?: Hooks | undefined);
    request<T>(document: any, dataPath?: string[], rootField?: string, typeName?: string, isList?: boolean, callsite?: string): Promise<T>;
    sanitizeMessage(message: string): string;
    protected unpack(document: any, data: any, path: string[], rootField?: string, isList?: boolean): any;
  }

  export const ModelName: {
    Message: 'Message',
    MessageStream: 'MessageStream'
  };

  export type ModelName = (typeof ModelName)[keyof typeof ModelName]


  export type Datasources = {
    db?: Datasource
  }

  export type RejectOnNotFound = boolean | ((error: Error) => Error)
  export type RejectPerModel = { [P in ModelName]?: RejectOnNotFound }
  export type RejectPerOperation =  { [P in "findUnique" | "findFirst"]?: RejectPerModel | RejectOnNotFound } 
  type IsReject<T> = T extends true ? True : T extends (err: Error) => Error ? True : False
  export type HasReject<
    GlobalRejectSettings extends Prisma.PrismaClientOptions['rejectOnNotFound'],
    LocalRejectSettings,
    Action extends PrismaAction,
    Model extends ModelName
  > = LocalRejectSettings extends RejectOnNotFound
    ? IsReject<LocalRejectSettings>
    : GlobalRejectSettings extends RejectPerOperation
    ? Action extends keyof GlobalRejectSettings
      ? GlobalRejectSettings[Action] extends boolean
        ? IsReject<GlobalRejectSettings[Action]>
        : GlobalRejectSettings[Action] extends RejectPerModel
        ? Model extends keyof GlobalRejectSettings[Action]
          ? IsReject<GlobalRejectSettings[Action][Model]>
          : False
        : False
      : False
    : IsReject<GlobalRejectSettings>
  export type ErrorFormat = 'pretty' | 'colorless' | 'minimal'

  export interface PrismaClientOptions {
    /**
     * Configure findUnique/findFirst to throw an error if the query returns null. 
     *  * @example
     * ```
     * // Reject on both findUnique/findFirst
     * rejectOnNotFound: true
     * // Reject only on findFirst with a custom error
     * rejectOnNotFound: { findFirst: (err) => new Error("Custom Error")}
     * // Reject on user.findUnique with a custom error
     * rejectOnNotFound: { findUnique: {User: (err) => new Error("User not found")}}
     * ```
     */
    rejectOnNotFound?: RejectOnNotFound | RejectPerOperation
    /**
     * Overwrites the datasource url from your prisma.schema file
     */
    datasources?: Datasources

    /**
     * @default "colorless"
     */
    errorFormat?: ErrorFormat

    /**
     * @example
     * ```
     * // Defaults to stdout
     * log: ['query', 'info', 'warn', 'error']
     * 
     * // Emit as events
     * log: [
     *  { emit: 'stdout', level: 'query' },
     *  { emit: 'stdout', level: 'info' },
     *  { emit: 'stdout', level: 'warn' }
     *  { emit: 'stdout', level: 'error' }
     * ]
     * ```
     * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/logging#the-log-option).
     */
    log?: Array<LogLevel | LogDefinition>
  }

  export type Hooks = {
    beforeRequest?: (options: { query: string, path: string[], rootField?: string, typeName?: string, document: any }) => any
  }

  /* Types for Logging */
  export type LogLevel = 'info' | 'query' | 'warn' | 'error'
  export type LogDefinition = {
    level: LogLevel
    emit: 'stdout' | 'event'
  }

  export type GetLogType<T extends LogLevel | LogDefinition> = T extends LogDefinition ? T['emit'] extends 'event' ? T['level'] : never : never
  export type GetEvents<T extends any> = T extends Array<LogLevel | LogDefinition> ?
    GetLogType<T[0]> | GetLogType<T[1]> | GetLogType<T[2]> | GetLogType<T[3]>
    : never

  export type QueryEvent = {
    timestamp: Date
    query: string
    params: string
    duration: number
    target: string
  }

  export type LogEvent = {
    timestamp: Date
    message: string
    target: string
  }
  /* End Types for Logging */


  export type PrismaAction =
    | 'findUnique'
    | 'findMany'
    | 'findFirst'
    | 'create'
    | 'createMany'
    | 'update'
    | 'updateMany'
    | 'upsert'
    | 'delete'
    | 'deleteMany'
    | 'executeRaw'
    | 'queryRaw'
    | 'aggregate'
    | 'count'

  /**
   * These options are being passed in to the middleware as "params"
   */
  export type MiddlewareParams = {
    model?: ModelName
    action: PrismaAction
    args: any
    dataPath: string[]
    runInTransaction: boolean
  }

  /**
   * The `T` type makes sure, that the `return proceed` is not forgotten in the middleware implementation
   */
  export type Middleware<T = any> = (
    params: MiddlewareParams,
    next: (params: MiddlewareParams) => Promise<T>,
  ) => Promise<T>

  // tested in getLogLevel.test.ts
  export function getLogLevel(log: Array<LogLevel | LogDefinition>): LogLevel | undefined; 
  export type Datasource = {
    url?: string
  }

  /**
   * Count Types
   */



  /**
   * Models
   */

  /**
   * Model Message
   */


  export type AggregateMessage = {
    _count: MessageCountAggregateOutputType | null
    count: MessageCountAggregateOutputType | null
    _avg: MessageAvgAggregateOutputType | null
    avg: MessageAvgAggregateOutputType | null
    _sum: MessageSumAggregateOutputType | null
    sum: MessageSumAggregateOutputType | null
    _min: MessageMinAggregateOutputType | null
    min: MessageMinAggregateOutputType | null
    _max: MessageMaxAggregateOutputType | null
    max: MessageMaxAggregateOutputType | null
  }

  export type MessageAvgAggregateOutputType = {
    globalPosition: number | null
  }

  export type MessageSumAggregateOutputType = {
    globalPosition: bigint | null
  }

  export type MessageMinAggregateOutputType = {
    id: string | null
    type: string | null
    globalPosition: bigint | null
    time: Date | null
  }

  export type MessageMaxAggregateOutputType = {
    id: string | null
    type: string | null
    globalPosition: bigint | null
    time: Date | null
  }

  export type MessageCountAggregateOutputType = {
    id: number
    type: number
    globalPosition: number
    data: number
    metadata: number
    time: number
    _all: number
  }


  export type MessageAvgAggregateInputType = {
    globalPosition?: true
  }

  export type MessageSumAggregateInputType = {
    globalPosition?: true
  }

  export type MessageMinAggregateInputType = {
    id?: true
    type?: true
    globalPosition?: true
    time?: true
  }

  export type MessageMaxAggregateInputType = {
    id?: true
    type?: true
    globalPosition?: true
    time?: true
  }

  export type MessageCountAggregateInputType = {
    id?: true
    type?: true
    globalPosition?: true
    data?: true
    metadata?: true
    time?: true
    _all?: true
  }

  export type MessageAggregateArgs = {
    /**
     * Filter which Message to aggregate.
     * 
    **/
    where?: MessageWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Messages to fetch.
     * 
    **/
    orderBy?: Enumerable<MessageOrderByInput>
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     * 
    **/
    cursor?: MessageWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Messages from the position of the cursor.
     * 
    **/
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Messages.
     * 
    **/
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned Messages
    **/
    _count?: true | MessageCountAggregateInputType
    /**
     * @deprecated since 2.23.0 please use `_count`
    **/
    count?: true | MessageCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: MessageAvgAggregateInputType
    /**
     * @deprecated since 2.23.0 please use `_avg`
    **/
    avg?: MessageAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: MessageSumAggregateInputType
    /**
     * @deprecated since 2.23.0 please use `_sum`
    **/
    sum?: MessageSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: MessageMinAggregateInputType
    /**
     * @deprecated since 2.23.0 please use `_min`
    **/
    min?: MessageMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: MessageMaxAggregateInputType
    /**
     * @deprecated since 2.23.0 please use `_max`
    **/
    max?: MessageMaxAggregateInputType
  }

  export type GetMessageAggregateType<T extends MessageAggregateArgs> = {
        [P in keyof T & keyof AggregateMessage]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateMessage[P]>
      : GetScalarType<T[P], AggregateMessage[P]>
  }


    
    
  export type MessageGroupByArgs = {
    where?: MessageWhereInput
    orderBy?: Enumerable<MessageOrderByInput>
    by: Array<MessageScalarFieldEnum>
    having?: MessageScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: MessageCountAggregateInputType | true
    _avg?: MessageAvgAggregateInputType
    _sum?: MessageSumAggregateInputType
    _min?: MessageMinAggregateInputType
    _max?: MessageMaxAggregateInputType
  }


  export type MessageGroupByOutputType = {
    id: string
    type: string
    globalPosition: bigint
    data: JsonValue | null
    metadata: JsonValue | null
    time: Date
    _count: MessageCountAggregateOutputType | null
    _avg: MessageAvgAggregateOutputType | null
    _sum: MessageSumAggregateOutputType | null
    _min: MessageMinAggregateOutputType | null
    _max: MessageMaxAggregateOutputType | null
  }

  type GetMessageGroupByPayload<T extends MessageGroupByArgs> = Promise<
    Array<
      PickArray<MessageGroupByOutputType, T['by']> & 
        {
          [P in ((keyof T) & (keyof MessageGroupByOutputType))]: P extends '_count' 
            ? T[P] extends boolean 
              ? number 
              : GetScalarType<T[P], MessageGroupByOutputType[P]> 
            : GetScalarType<T[P], MessageGroupByOutputType[P]>
        }
      > 
    >


  export type MessageSelect = {
    id?: boolean
    type?: boolean
    globalPosition?: boolean
    data?: boolean
    metadata?: boolean
    time?: boolean
    streams?: boolean | MessageStreamFindManyArgs
  }

  export type MessageInclude = {
    streams?: boolean | MessageStreamFindManyArgs
  }

  export type MessageGetPayload<
    S extends boolean | null | undefined | MessageArgs,
    U = keyof S
      > = S extends true
        ? Message
    : S extends undefined
    ? never
    : S extends MessageArgs | MessageFindManyArgs
    ?'include' extends U
    ? Message  & {
    [P in TrueKeys<S['include']>]: 
          P extends 'streams'
        ? Array < MessageStreamGetPayload<S['include'][P]>>  : never
  } 
    : 'select' extends U
    ? {
    [P in TrueKeys<S['select']>]: P extends keyof Message ?Message [P]
  : 
          P extends 'streams'
        ? Array < MessageStreamGetPayload<S['select'][P]>>  : never
  } 
    : Message
  : Message


  type MessageCountArgs = Merge<
    Omit<MessageFindManyArgs, 'select' | 'include'> & {
      select?: MessageCountAggregateInputType | true
    }
  >

  export interface MessageDelegate<GlobalRejectSettings> {
    /**
     * Find zero or one Message that matches the filter.
     * @param {MessageFindUniqueArgs} args - Arguments to find a Message
     * @example
     * // Get one Message
     * const message = await prisma.message.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends MessageFindUniqueArgs,  LocalRejectSettings = T["rejectOnNotFound"] extends RejectOnNotFound ? T['rejectOnNotFound'] : undefined>(
      args: SelectSubset<T, MessageFindUniqueArgs>
    ): HasReject<GlobalRejectSettings, LocalRejectSettings, 'findUnique', 'Message'> extends True ? CheckSelect<T, Prisma__MessageClient<Message>, Prisma__MessageClient<MessageGetPayload<T>>> : CheckSelect<T, Prisma__MessageClient<Message | null >, Prisma__MessageClient<MessageGetPayload<T> | null >>

    /**
     * Find the first Message that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {MessageFindFirstArgs} args - Arguments to find a Message
     * @example
     * // Get one Message
     * const message = await prisma.message.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends MessageFindFirstArgs,  LocalRejectSettings = T["rejectOnNotFound"] extends RejectOnNotFound ? T['rejectOnNotFound'] : undefined>(
      args?: SelectSubset<T, MessageFindFirstArgs>
    ): HasReject<GlobalRejectSettings, LocalRejectSettings, 'findFirst', 'Message'> extends True ? CheckSelect<T, Prisma__MessageClient<Message>, Prisma__MessageClient<MessageGetPayload<T>>> : CheckSelect<T, Prisma__MessageClient<Message | null >, Prisma__MessageClient<MessageGetPayload<T> | null >>

    /**
     * Find zero or more Messages that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {MessageFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all Messages
     * const messages = await prisma.message.findMany()
     * 
     * // Get first 10 Messages
     * const messages = await prisma.message.findMany({ take: 10 })
     * 
     * // Only select the `id`
     * const messageWithIdOnly = await prisma.message.findMany({ select: { id: true } })
     * 
    **/
    findMany<T extends MessageFindManyArgs>(
      args?: SelectSubset<T, MessageFindManyArgs>
    ): CheckSelect<T, PrismaPromise<Array<Message>>, PrismaPromise<Array<MessageGetPayload<T>>>>

    /**
     * Create a Message.
     * @param {MessageCreateArgs} args - Arguments to create a Message.
     * @example
     * // Create one Message
     * const Message = await prisma.message.create({
     *   data: {
     *     // ... data to create a Message
     *   }
     * })
     * 
    **/
    create<T extends MessageCreateArgs>(
      args: SelectSubset<T, MessageCreateArgs>
    ): CheckSelect<T, Prisma__MessageClient<Message>, Prisma__MessageClient<MessageGetPayload<T>>>

    /**
     * Create many Messages.
     *     @param {MessageCreateManyArgs} args - Arguments to create many Messages.
     *     @example
     *     // Create many Messages
     *     const message = await prisma.message.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends MessageCreateManyArgs>(
      args?: SelectSubset<T, MessageCreateManyArgs>
    ): PrismaPromise<BatchPayload>

    /**
     * Delete a Message.
     * @param {MessageDeleteArgs} args - Arguments to delete one Message.
     * @example
     * // Delete one Message
     * const Message = await prisma.message.delete({
     *   where: {
     *     // ... filter to delete one Message
     *   }
     * })
     * 
    **/
    delete<T extends MessageDeleteArgs>(
      args: SelectSubset<T, MessageDeleteArgs>
    ): CheckSelect<T, Prisma__MessageClient<Message>, Prisma__MessageClient<MessageGetPayload<T>>>

    /**
     * Update one Message.
     * @param {MessageUpdateArgs} args - Arguments to update one Message.
     * @example
     * // Update one Message
     * const message = await prisma.message.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends MessageUpdateArgs>(
      args: SelectSubset<T, MessageUpdateArgs>
    ): CheckSelect<T, Prisma__MessageClient<Message>, Prisma__MessageClient<MessageGetPayload<T>>>

    /**
     * Delete zero or more Messages.
     * @param {MessageDeleteManyArgs} args - Arguments to filter Messages to delete.
     * @example
     * // Delete a few Messages
     * const { count } = await prisma.message.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends MessageDeleteManyArgs>(
      args?: SelectSubset<T, MessageDeleteManyArgs>
    ): PrismaPromise<BatchPayload>

    /**
     * Update zero or more Messages.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {MessageUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many Messages
     * const message = await prisma.message.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends MessageUpdateManyArgs>(
      args: SelectSubset<T, MessageUpdateManyArgs>
    ): PrismaPromise<BatchPayload>

    /**
     * Create or update one Message.
     * @param {MessageUpsertArgs} args - Arguments to update or create a Message.
     * @example
     * // Update or create a Message
     * const message = await prisma.message.upsert({
     *   create: {
     *     // ... data to create a Message
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the Message we want to update
     *   }
     * })
    **/
    upsert<T extends MessageUpsertArgs>(
      args: SelectSubset<T, MessageUpsertArgs>
    ): CheckSelect<T, Prisma__MessageClient<Message>, Prisma__MessageClient<MessageGetPayload<T>>>

    /**
     * Count the number of Messages.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {MessageCountArgs} args - Arguments to filter Messages to count.
     * @example
     * // Count the number of Messages
     * const count = await prisma.message.count({
     *   where: {
     *     // ... the filter for the Messages we want to count
     *   }
     * })
    **/
    count<T extends MessageCountArgs>(
      args?: Subset<T, MessageCountArgs>,
    ): PrismaPromise<
      T extends _Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], MessageCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a Message.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {MessageAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends MessageAggregateArgs>(args: Subset<T, MessageAggregateArgs>): PrismaPromise<GetMessageAggregateType<T>>

    /**
     * Group by Message.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {MessageGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends MessageGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: MessageGroupByArgs['orderBy'] }
        : { orderBy?: MessageGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends TupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, MessageGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetMessageGroupByPayload<T> : Promise<InputErrors>
  }

  /**
   * The delegate class that acts as a "Promise-like" for Message.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in 
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export class Prisma__MessageClient<T> implements PrismaPromise<T> {
    [prisma]: true;
    private readonly _dmmf;
    private readonly _fetcher;
    private readonly _queryType;
    private readonly _rootField;
    private readonly _clientMethod;
    private readonly _args;
    private readonly _dataPath;
    private readonly _errorFormat;
    private readonly _measurePerformance?;
    private _isList;
    private _callsite;
    private _requestPromise?;
    constructor(_dmmf: runtime.DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
    readonly [Symbol.toStringTag]: 'PrismaClientPromise';

    streams<T extends MessageStreamFindManyArgs = {}>(args?: Subset<T, MessageStreamFindManyArgs>): CheckSelect<T, PrismaPromise<Array<MessageStream>>, PrismaPromise<Array<MessageStreamGetPayload<T>>>>;

    private get _document();
    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): Promise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): Promise<T>;
  }

  // Custom InputTypes

  /**
   * Message findUnique
   */
  export type MessageFindUniqueArgs = {
    /**
     * Select specific fields to fetch from the Message
     * 
    **/
    select?: MessageSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageInclude | null
    /**
     * Throw an Error if a Message can't be found
     * 
    **/
    rejectOnNotFound?: RejectOnNotFound
    /**
     * Filter, which Message to fetch.
     * 
    **/
    where: MessageWhereUniqueInput
  }


  /**
   * Message findFirst
   */
  export type MessageFindFirstArgs = {
    /**
     * Select specific fields to fetch from the Message
     * 
    **/
    select?: MessageSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageInclude | null
    /**
     * Throw an Error if a Message can't be found
     * 
    **/
    rejectOnNotFound?: RejectOnNotFound
    /**
     * Filter, which Message to fetch.
     * 
    **/
    where?: MessageWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Messages to fetch.
     * 
    **/
    orderBy?: Enumerable<MessageOrderByInput>
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Messages.
     * 
    **/
    cursor?: MessageWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Messages from the position of the cursor.
     * 
    **/
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Messages.
     * 
    **/
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Messages.
     * 
    **/
    distinct?: Enumerable<MessageScalarFieldEnum>
  }


  /**
   * Message findMany
   */
  export type MessageFindManyArgs = {
    /**
     * Select specific fields to fetch from the Message
     * 
    **/
    select?: MessageSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageInclude | null
    /**
     * Filter, which Messages to fetch.
     * 
    **/
    where?: MessageWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Messages to fetch.
     * 
    **/
    orderBy?: Enumerable<MessageOrderByInput>
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing Messages.
     * 
    **/
    cursor?: MessageWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Messages from the position of the cursor.
     * 
    **/
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Messages.
     * 
    **/
    skip?: number
    distinct?: Enumerable<MessageScalarFieldEnum>
  }


  /**
   * Message create
   */
  export type MessageCreateArgs = {
    /**
     * Select specific fields to fetch from the Message
     * 
    **/
    select?: MessageSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageInclude | null
    /**
     * The data needed to create a Message.
     * 
    **/
    data: XOR<MessageCreateInput, MessageUncheckedCreateInput>
  }


  /**
   * Message createMany
   */
  export type MessageCreateManyArgs = {
    data: Enumerable<MessageCreateManyInput>
    skipDuplicates?: boolean
  }


  /**
   * Message update
   */
  export type MessageUpdateArgs = {
    /**
     * Select specific fields to fetch from the Message
     * 
    **/
    select?: MessageSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageInclude | null
    /**
     * The data needed to update a Message.
     * 
    **/
    data: XOR<MessageUpdateInput, MessageUncheckedUpdateInput>
    /**
     * Choose, which Message to update.
     * 
    **/
    where: MessageWhereUniqueInput
  }


  /**
   * Message updateMany
   */
  export type MessageUpdateManyArgs = {
    data: XOR<MessageUpdateManyMutationInput, MessageUncheckedUpdateManyInput>
    where?: MessageWhereInput
  }


  /**
   * Message upsert
   */
  export type MessageUpsertArgs = {
    /**
     * Select specific fields to fetch from the Message
     * 
    **/
    select?: MessageSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageInclude | null
    /**
     * The filter to search for the Message to update in case it exists.
     * 
    **/
    where: MessageWhereUniqueInput
    /**
     * In case the Message found by the `where` argument doesn't exist, create a new Message with this data.
     * 
    **/
    create: XOR<MessageCreateInput, MessageUncheckedCreateInput>
    /**
     * In case the Message was found with the provided `where` argument, update it with this data.
     * 
    **/
    update: XOR<MessageUpdateInput, MessageUncheckedUpdateInput>
  }


  /**
   * Message delete
   */
  export type MessageDeleteArgs = {
    /**
     * Select specific fields to fetch from the Message
     * 
    **/
    select?: MessageSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageInclude | null
    /**
     * Filter which Message to delete.
     * 
    **/
    where: MessageWhereUniqueInput
  }


  /**
   * Message deleteMany
   */
  export type MessageDeleteManyArgs = {
    where?: MessageWhereInput
  }


  /**
   * Message without action
   */
  export type MessageArgs = {
    /**
     * Select specific fields to fetch from the Message
     * 
    **/
    select?: MessageSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageInclude | null
  }



  /**
   * Model MessageStream
   */


  export type AggregateMessageStream = {
    _count: MessageStreamCountAggregateOutputType | null
    count: MessageStreamCountAggregateOutputType | null
    _avg: MessageStreamAvgAggregateOutputType | null
    avg: MessageStreamAvgAggregateOutputType | null
    _sum: MessageStreamSumAggregateOutputType | null
    sum: MessageStreamSumAggregateOutputType | null
    _min: MessageStreamMinAggregateOutputType | null
    min: MessageStreamMinAggregateOutputType | null
    _max: MessageStreamMaxAggregateOutputType | null
    max: MessageStreamMaxAggregateOutputType | null
  }

  export type MessageStreamAvgAggregateOutputType = {
    position: number | null
  }

  export type MessageStreamSumAggregateOutputType = {
    position: bigint | null
  }

  export type MessageStreamMinAggregateOutputType = {
    messageId: string | null
    streamName: string | null
    position: bigint | null
  }

  export type MessageStreamMaxAggregateOutputType = {
    messageId: string | null
    streamName: string | null
    position: bigint | null
  }

  export type MessageStreamCountAggregateOutputType = {
    messageId: number
    streamName: number
    position: number
    _all: number
  }


  export type MessageStreamAvgAggregateInputType = {
    position?: true
  }

  export type MessageStreamSumAggregateInputType = {
    position?: true
  }

  export type MessageStreamMinAggregateInputType = {
    messageId?: true
    streamName?: true
    position?: true
  }

  export type MessageStreamMaxAggregateInputType = {
    messageId?: true
    streamName?: true
    position?: true
  }

  export type MessageStreamCountAggregateInputType = {
    messageId?: true
    streamName?: true
    position?: true
    _all?: true
  }

  export type MessageStreamAggregateArgs = {
    /**
     * Filter which MessageStream to aggregate.
     * 
    **/
    where?: MessageStreamWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of MessageStreams to fetch.
     * 
    **/
    orderBy?: Enumerable<MessageStreamOrderByInput>
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     * 
    **/
    cursor?: MessageStreamWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` MessageStreams from the position of the cursor.
     * 
    **/
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` MessageStreams.
     * 
    **/
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned MessageStreams
    **/
    _count?: true | MessageStreamCountAggregateInputType
    /**
     * @deprecated since 2.23.0 please use `_count`
    **/
    count?: true | MessageStreamCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: MessageStreamAvgAggregateInputType
    /**
     * @deprecated since 2.23.0 please use `_avg`
    **/
    avg?: MessageStreamAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: MessageStreamSumAggregateInputType
    /**
     * @deprecated since 2.23.0 please use `_sum`
    **/
    sum?: MessageStreamSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: MessageStreamMinAggregateInputType
    /**
     * @deprecated since 2.23.0 please use `_min`
    **/
    min?: MessageStreamMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: MessageStreamMaxAggregateInputType
    /**
     * @deprecated since 2.23.0 please use `_max`
    **/
    max?: MessageStreamMaxAggregateInputType
  }

  export type GetMessageStreamAggregateType<T extends MessageStreamAggregateArgs> = {
        [P in keyof T & keyof AggregateMessageStream]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateMessageStream[P]>
      : GetScalarType<T[P], AggregateMessageStream[P]>
  }


    
    
  export type MessageStreamGroupByArgs = {
    where?: MessageStreamWhereInput
    orderBy?: Enumerable<MessageStreamOrderByInput>
    by: Array<MessageStreamScalarFieldEnum>
    having?: MessageStreamScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: MessageStreamCountAggregateInputType | true
    _avg?: MessageStreamAvgAggregateInputType
    _sum?: MessageStreamSumAggregateInputType
    _min?: MessageStreamMinAggregateInputType
    _max?: MessageStreamMaxAggregateInputType
  }


  export type MessageStreamGroupByOutputType = {
    messageId: string
    streamName: string
    position: bigint
    _count: MessageStreamCountAggregateOutputType | null
    _avg: MessageStreamAvgAggregateOutputType | null
    _sum: MessageStreamSumAggregateOutputType | null
    _min: MessageStreamMinAggregateOutputType | null
    _max: MessageStreamMaxAggregateOutputType | null
  }

  type GetMessageStreamGroupByPayload<T extends MessageStreamGroupByArgs> = Promise<
    Array<
      PickArray<MessageStreamGroupByOutputType, T['by']> & 
        {
          [P in ((keyof T) & (keyof MessageStreamGroupByOutputType))]: P extends '_count' 
            ? T[P] extends boolean 
              ? number 
              : GetScalarType<T[P], MessageStreamGroupByOutputType[P]> 
            : GetScalarType<T[P], MessageStreamGroupByOutputType[P]>
        }
      > 
    >


  export type MessageStreamSelect = {
    messageId?: boolean
    streamName?: boolean
    position?: boolean
    message?: boolean | MessageArgs
  }

  export type MessageStreamInclude = {
    message?: boolean | MessageArgs
  }

  export type MessageStreamGetPayload<
    S extends boolean | null | undefined | MessageStreamArgs,
    U = keyof S
      > = S extends true
        ? MessageStream
    : S extends undefined
    ? never
    : S extends MessageStreamArgs | MessageStreamFindManyArgs
    ?'include' extends U
    ? MessageStream  & {
    [P in TrueKeys<S['include']>]: 
          P extends 'message'
        ? MessageGetPayload<S['include'][P]> : never
  } 
    : 'select' extends U
    ? {
    [P in TrueKeys<S['select']>]: P extends keyof MessageStream ?MessageStream [P]
  : 
          P extends 'message'
        ? MessageGetPayload<S['select'][P]> : never
  } 
    : MessageStream
  : MessageStream


  type MessageStreamCountArgs = Merge<
    Omit<MessageStreamFindManyArgs, 'select' | 'include'> & {
      select?: MessageStreamCountAggregateInputType | true
    }
  >

  export interface MessageStreamDelegate<GlobalRejectSettings> {
    /**
     * Find zero or one MessageStream that matches the filter.
     * @param {MessageStreamFindUniqueArgs} args - Arguments to find a MessageStream
     * @example
     * // Get one MessageStream
     * const messageStream = await prisma.messageStream.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends MessageStreamFindUniqueArgs,  LocalRejectSettings = T["rejectOnNotFound"] extends RejectOnNotFound ? T['rejectOnNotFound'] : undefined>(
      args: SelectSubset<T, MessageStreamFindUniqueArgs>
    ): HasReject<GlobalRejectSettings, LocalRejectSettings, 'findUnique', 'MessageStream'> extends True ? CheckSelect<T, Prisma__MessageStreamClient<MessageStream>, Prisma__MessageStreamClient<MessageStreamGetPayload<T>>> : CheckSelect<T, Prisma__MessageStreamClient<MessageStream | null >, Prisma__MessageStreamClient<MessageStreamGetPayload<T> | null >>

    /**
     * Find the first MessageStream that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {MessageStreamFindFirstArgs} args - Arguments to find a MessageStream
     * @example
     * // Get one MessageStream
     * const messageStream = await prisma.messageStream.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends MessageStreamFindFirstArgs,  LocalRejectSettings = T["rejectOnNotFound"] extends RejectOnNotFound ? T['rejectOnNotFound'] : undefined>(
      args?: SelectSubset<T, MessageStreamFindFirstArgs>
    ): HasReject<GlobalRejectSettings, LocalRejectSettings, 'findFirst', 'MessageStream'> extends True ? CheckSelect<T, Prisma__MessageStreamClient<MessageStream>, Prisma__MessageStreamClient<MessageStreamGetPayload<T>>> : CheckSelect<T, Prisma__MessageStreamClient<MessageStream | null >, Prisma__MessageStreamClient<MessageStreamGetPayload<T> | null >>

    /**
     * Find zero or more MessageStreams that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {MessageStreamFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all MessageStreams
     * const messageStreams = await prisma.messageStream.findMany()
     * 
     * // Get first 10 MessageStreams
     * const messageStreams = await prisma.messageStream.findMany({ take: 10 })
     * 
     * // Only select the `messageId`
     * const messageStreamWithMessageIdOnly = await prisma.messageStream.findMany({ select: { messageId: true } })
     * 
    **/
    findMany<T extends MessageStreamFindManyArgs>(
      args?: SelectSubset<T, MessageStreamFindManyArgs>
    ): CheckSelect<T, PrismaPromise<Array<MessageStream>>, PrismaPromise<Array<MessageStreamGetPayload<T>>>>

    /**
     * Create a MessageStream.
     * @param {MessageStreamCreateArgs} args - Arguments to create a MessageStream.
     * @example
     * // Create one MessageStream
     * const MessageStream = await prisma.messageStream.create({
     *   data: {
     *     // ... data to create a MessageStream
     *   }
     * })
     * 
    **/
    create<T extends MessageStreamCreateArgs>(
      args: SelectSubset<T, MessageStreamCreateArgs>
    ): CheckSelect<T, Prisma__MessageStreamClient<MessageStream>, Prisma__MessageStreamClient<MessageStreamGetPayload<T>>>

    /**
     * Create many MessageStreams.
     *     @param {MessageStreamCreateManyArgs} args - Arguments to create many MessageStreams.
     *     @example
     *     // Create many MessageStreams
     *     const messageStream = await prisma.messageStream.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends MessageStreamCreateManyArgs>(
      args?: SelectSubset<T, MessageStreamCreateManyArgs>
    ): PrismaPromise<BatchPayload>

    /**
     * Delete a MessageStream.
     * @param {MessageStreamDeleteArgs} args - Arguments to delete one MessageStream.
     * @example
     * // Delete one MessageStream
     * const MessageStream = await prisma.messageStream.delete({
     *   where: {
     *     // ... filter to delete one MessageStream
     *   }
     * })
     * 
    **/
    delete<T extends MessageStreamDeleteArgs>(
      args: SelectSubset<T, MessageStreamDeleteArgs>
    ): CheckSelect<T, Prisma__MessageStreamClient<MessageStream>, Prisma__MessageStreamClient<MessageStreamGetPayload<T>>>

    /**
     * Update one MessageStream.
     * @param {MessageStreamUpdateArgs} args - Arguments to update one MessageStream.
     * @example
     * // Update one MessageStream
     * const messageStream = await prisma.messageStream.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends MessageStreamUpdateArgs>(
      args: SelectSubset<T, MessageStreamUpdateArgs>
    ): CheckSelect<T, Prisma__MessageStreamClient<MessageStream>, Prisma__MessageStreamClient<MessageStreamGetPayload<T>>>

    /**
     * Delete zero or more MessageStreams.
     * @param {MessageStreamDeleteManyArgs} args - Arguments to filter MessageStreams to delete.
     * @example
     * // Delete a few MessageStreams
     * const { count } = await prisma.messageStream.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends MessageStreamDeleteManyArgs>(
      args?: SelectSubset<T, MessageStreamDeleteManyArgs>
    ): PrismaPromise<BatchPayload>

    /**
     * Update zero or more MessageStreams.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {MessageStreamUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many MessageStreams
     * const messageStream = await prisma.messageStream.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends MessageStreamUpdateManyArgs>(
      args: SelectSubset<T, MessageStreamUpdateManyArgs>
    ): PrismaPromise<BatchPayload>

    /**
     * Create or update one MessageStream.
     * @param {MessageStreamUpsertArgs} args - Arguments to update or create a MessageStream.
     * @example
     * // Update or create a MessageStream
     * const messageStream = await prisma.messageStream.upsert({
     *   create: {
     *     // ... data to create a MessageStream
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the MessageStream we want to update
     *   }
     * })
    **/
    upsert<T extends MessageStreamUpsertArgs>(
      args: SelectSubset<T, MessageStreamUpsertArgs>
    ): CheckSelect<T, Prisma__MessageStreamClient<MessageStream>, Prisma__MessageStreamClient<MessageStreamGetPayload<T>>>

    /**
     * Count the number of MessageStreams.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {MessageStreamCountArgs} args - Arguments to filter MessageStreams to count.
     * @example
     * // Count the number of MessageStreams
     * const count = await prisma.messageStream.count({
     *   where: {
     *     // ... the filter for the MessageStreams we want to count
     *   }
     * })
    **/
    count<T extends MessageStreamCountArgs>(
      args?: Subset<T, MessageStreamCountArgs>,
    ): PrismaPromise<
      T extends _Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], MessageStreamCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a MessageStream.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {MessageStreamAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends MessageStreamAggregateArgs>(args: Subset<T, MessageStreamAggregateArgs>): PrismaPromise<GetMessageStreamAggregateType<T>>

    /**
     * Group by MessageStream.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {MessageStreamGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends MessageStreamGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: MessageStreamGroupByArgs['orderBy'] }
        : { orderBy?: MessageStreamGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends TupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, MessageStreamGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetMessageStreamGroupByPayload<T> : Promise<InputErrors>
  }

  /**
   * The delegate class that acts as a "Promise-like" for MessageStream.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in 
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export class Prisma__MessageStreamClient<T> implements PrismaPromise<T> {
    [prisma]: true;
    private readonly _dmmf;
    private readonly _fetcher;
    private readonly _queryType;
    private readonly _rootField;
    private readonly _clientMethod;
    private readonly _args;
    private readonly _dataPath;
    private readonly _errorFormat;
    private readonly _measurePerformance?;
    private _isList;
    private _callsite;
    private _requestPromise?;
    constructor(_dmmf: runtime.DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
    readonly [Symbol.toStringTag]: 'PrismaClientPromise';

    message<T extends MessageArgs = {}>(args?: Subset<T, MessageArgs>): CheckSelect<T, Prisma__MessageClient<Message | null >, Prisma__MessageClient<MessageGetPayload<T> | null >>;

    private get _document();
    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): Promise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): Promise<T>;
  }

  // Custom InputTypes

  /**
   * MessageStream findUnique
   */
  export type MessageStreamFindUniqueArgs = {
    /**
     * Select specific fields to fetch from the MessageStream
     * 
    **/
    select?: MessageStreamSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageStreamInclude | null
    /**
     * Throw an Error if a MessageStream can't be found
     * 
    **/
    rejectOnNotFound?: RejectOnNotFound
    /**
     * Filter, which MessageStream to fetch.
     * 
    **/
    where: MessageStreamWhereUniqueInput
  }


  /**
   * MessageStream findFirst
   */
  export type MessageStreamFindFirstArgs = {
    /**
     * Select specific fields to fetch from the MessageStream
     * 
    **/
    select?: MessageStreamSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageStreamInclude | null
    /**
     * Throw an Error if a MessageStream can't be found
     * 
    **/
    rejectOnNotFound?: RejectOnNotFound
    /**
     * Filter, which MessageStream to fetch.
     * 
    **/
    where?: MessageStreamWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of MessageStreams to fetch.
     * 
    **/
    orderBy?: Enumerable<MessageStreamOrderByInput>
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for MessageStreams.
     * 
    **/
    cursor?: MessageStreamWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` MessageStreams from the position of the cursor.
     * 
    **/
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` MessageStreams.
     * 
    **/
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of MessageStreams.
     * 
    **/
    distinct?: Enumerable<MessageStreamScalarFieldEnum>
  }


  /**
   * MessageStream findMany
   */
  export type MessageStreamFindManyArgs = {
    /**
     * Select specific fields to fetch from the MessageStream
     * 
    **/
    select?: MessageStreamSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageStreamInclude | null
    /**
     * Filter, which MessageStreams to fetch.
     * 
    **/
    where?: MessageStreamWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of MessageStreams to fetch.
     * 
    **/
    orderBy?: Enumerable<MessageStreamOrderByInput>
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing MessageStreams.
     * 
    **/
    cursor?: MessageStreamWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` MessageStreams from the position of the cursor.
     * 
    **/
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` MessageStreams.
     * 
    **/
    skip?: number
    distinct?: Enumerable<MessageStreamScalarFieldEnum>
  }


  /**
   * MessageStream create
   */
  export type MessageStreamCreateArgs = {
    /**
     * Select specific fields to fetch from the MessageStream
     * 
    **/
    select?: MessageStreamSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageStreamInclude | null
    /**
     * The data needed to create a MessageStream.
     * 
    **/
    data: XOR<MessageStreamCreateInput, MessageStreamUncheckedCreateInput>
  }


  /**
   * MessageStream createMany
   */
  export type MessageStreamCreateManyArgs = {
    data: Enumerable<MessageStreamCreateManyInput>
    skipDuplicates?: boolean
  }


  /**
   * MessageStream update
   */
  export type MessageStreamUpdateArgs = {
    /**
     * Select specific fields to fetch from the MessageStream
     * 
    **/
    select?: MessageStreamSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageStreamInclude | null
    /**
     * The data needed to update a MessageStream.
     * 
    **/
    data: XOR<MessageStreamUpdateInput, MessageStreamUncheckedUpdateInput>
    /**
     * Choose, which MessageStream to update.
     * 
    **/
    where: MessageStreamWhereUniqueInput
  }


  /**
   * MessageStream updateMany
   */
  export type MessageStreamUpdateManyArgs = {
    data: XOR<MessageStreamUpdateManyMutationInput, MessageStreamUncheckedUpdateManyInput>
    where?: MessageStreamWhereInput
  }


  /**
   * MessageStream upsert
   */
  export type MessageStreamUpsertArgs = {
    /**
     * Select specific fields to fetch from the MessageStream
     * 
    **/
    select?: MessageStreamSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageStreamInclude | null
    /**
     * The filter to search for the MessageStream to update in case it exists.
     * 
    **/
    where: MessageStreamWhereUniqueInput
    /**
     * In case the MessageStream found by the `where` argument doesn't exist, create a new MessageStream with this data.
     * 
    **/
    create: XOR<MessageStreamCreateInput, MessageStreamUncheckedCreateInput>
    /**
     * In case the MessageStream was found with the provided `where` argument, update it with this data.
     * 
    **/
    update: XOR<MessageStreamUpdateInput, MessageStreamUncheckedUpdateInput>
  }


  /**
   * MessageStream delete
   */
  export type MessageStreamDeleteArgs = {
    /**
     * Select specific fields to fetch from the MessageStream
     * 
    **/
    select?: MessageStreamSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageStreamInclude | null
    /**
     * Filter which MessageStream to delete.
     * 
    **/
    where: MessageStreamWhereUniqueInput
  }


  /**
   * MessageStream deleteMany
   */
  export type MessageStreamDeleteManyArgs = {
    where?: MessageStreamWhereInput
  }


  /**
   * MessageStream without action
   */
  export type MessageStreamArgs = {
    /**
     * Select specific fields to fetch from the MessageStream
     * 
    **/
    select?: MessageStreamSelect | null
    /**
     * Choose, which related nodes to fetch as well.
     * 
    **/
    include?: MessageStreamInclude | null
  }



  /**
   * Enums
   */

  // Based on
  // https://github.com/microsoft/TypeScript/issues/3192#issuecomment-261720275

  export const MessageScalarFieldEnum: {
    id: 'id',
    type: 'type',
    globalPosition: 'globalPosition',
    data: 'data',
    metadata: 'metadata',
    time: 'time'
  };

  export type MessageScalarFieldEnum = (typeof MessageScalarFieldEnum)[keyof typeof MessageScalarFieldEnum]


  export const MessageStreamScalarFieldEnum: {
    messageId: 'messageId',
    streamName: 'streamName',
    position: 'position'
  };

  export type MessageStreamScalarFieldEnum = (typeof MessageStreamScalarFieldEnum)[keyof typeof MessageStreamScalarFieldEnum]


  export const SortOrder: {
    asc: 'asc',
    desc: 'desc'
  };

  export type SortOrder = (typeof SortOrder)[keyof typeof SortOrder]


  export const QueryMode: {
    default: 'default',
    insensitive: 'insensitive'
  };

  export type QueryMode = (typeof QueryMode)[keyof typeof QueryMode]


  /**
   * Deep Input Types
   */


  export type MessageWhereInput = {
    AND?: Enumerable<MessageWhereInput>
    OR?: Enumerable<MessageWhereInput>
    NOT?: Enumerable<MessageWhereInput>
    id?: StringFilter | string
    type?: StringFilter | string
    globalPosition?: BigIntFilter | bigint | number
    data?: JsonNullableFilter
    metadata?: JsonNullableFilter
    time?: DateTimeFilter | Date | string
    streams?: MessageStreamListRelationFilter
  }

  export type MessageOrderByInput = {
    id?: SortOrder
    type?: SortOrder
    globalPosition?: SortOrder
    data?: SortOrder
    metadata?: SortOrder
    time?: SortOrder
  }

  export type MessageWhereUniqueInput = {
    id?: string
    globalPosition?: bigint | number
  }

  export type MessageScalarWhereWithAggregatesInput = {
    AND?: Enumerable<MessageScalarWhereWithAggregatesInput>
    OR?: Enumerable<MessageScalarWhereWithAggregatesInput>
    NOT?: Enumerable<MessageScalarWhereWithAggregatesInput>
    id?: StringWithAggregatesFilter | string
    type?: StringWithAggregatesFilter | string
    globalPosition?: BigIntWithAggregatesFilter | bigint | number
    data?: JsonNullableWithAggregatesFilter
    metadata?: JsonNullableWithAggregatesFilter
    time?: DateTimeWithAggregatesFilter | Date | string
  }

  export type MessageStreamWhereInput = {
    AND?: Enumerable<MessageStreamWhereInput>
    OR?: Enumerable<MessageStreamWhereInput>
    NOT?: Enumerable<MessageStreamWhereInput>
    messageId?: StringFilter | string
    streamName?: StringFilter | string
    position?: BigIntFilter | bigint | number
    message?: XOR<MessageRelationFilter, MessageWhereInput>
  }

  export type MessageStreamOrderByInput = {
    messageId?: SortOrder
    streamName?: SortOrder
    position?: SortOrder
  }

  export type MessageStreamWhereUniqueInput = {
    messages_stream_position?: MessageStreamMessages_stream_positionCompoundUniqueInput
    messageId_streamName?: MessageStreamMessageIdStreamNameCompoundUniqueInput
  }

  export type MessageStreamScalarWhereWithAggregatesInput = {
    AND?: Enumerable<MessageStreamScalarWhereWithAggregatesInput>
    OR?: Enumerable<MessageStreamScalarWhereWithAggregatesInput>
    NOT?: Enumerable<MessageStreamScalarWhereWithAggregatesInput>
    messageId?: StringWithAggregatesFilter | string
    streamName?: StringWithAggregatesFilter | string
    position?: BigIntWithAggregatesFilter | bigint | number
  }

  export type MessageCreateInput = {
    id?: string
    type: string
    globalPosition?: bigint | number
    data?: InputJsonValue | null
    metadata?: InputJsonValue | null
    time?: Date | string
    streams?: MessageStreamCreateNestedManyWithoutMessageInput
  }

  export type MessageUncheckedCreateInput = {
    id?: string
    type: string
    globalPosition?: bigint | number
    data?: InputJsonValue | null
    metadata?: InputJsonValue | null
    time?: Date | string
    streams?: MessageStreamUncheckedCreateNestedManyWithoutMessageInput
  }

  export type MessageUpdateInput = {
    id?: StringFieldUpdateOperationsInput | string
    type?: StringFieldUpdateOperationsInput | string
    globalPosition?: BigIntFieldUpdateOperationsInput | bigint | number
    data?: InputJsonValue | null
    metadata?: InputJsonValue | null
    time?: DateTimeFieldUpdateOperationsInput | Date | string
    streams?: MessageStreamUpdateManyWithoutMessageInput
  }

  export type MessageUncheckedUpdateInput = {
    id?: StringFieldUpdateOperationsInput | string
    type?: StringFieldUpdateOperationsInput | string
    globalPosition?: BigIntFieldUpdateOperationsInput | bigint | number
    data?: InputJsonValue | null
    metadata?: InputJsonValue | null
    time?: DateTimeFieldUpdateOperationsInput | Date | string
    streams?: MessageStreamUncheckedUpdateManyWithoutMessageInput
  }

  export type MessageCreateManyInput = {
    id?: string
    type: string
    globalPosition?: bigint | number
    data?: InputJsonValue | null
    metadata?: InputJsonValue | null
    time?: Date | string
  }

  export type MessageUpdateManyMutationInput = {
    id?: StringFieldUpdateOperationsInput | string
    type?: StringFieldUpdateOperationsInput | string
    globalPosition?: BigIntFieldUpdateOperationsInput | bigint | number
    data?: InputJsonValue | null
    metadata?: InputJsonValue | null
    time?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type MessageUncheckedUpdateManyInput = {
    id?: StringFieldUpdateOperationsInput | string
    type?: StringFieldUpdateOperationsInput | string
    globalPosition?: BigIntFieldUpdateOperationsInput | bigint | number
    data?: InputJsonValue | null
    metadata?: InputJsonValue | null
    time?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type MessageStreamCreateInput = {
    streamName: string
    position: bigint | number
    message: MessageCreateNestedOneWithoutStreamsInput
  }

  export type MessageStreamUncheckedCreateInput = {
    messageId: string
    streamName: string
    position: bigint | number
  }

  export type MessageStreamUpdateInput = {
    streamName?: StringFieldUpdateOperationsInput | string
    position?: BigIntFieldUpdateOperationsInput | bigint | number
    message?: MessageUpdateOneRequiredWithoutStreamsInput
  }

  export type MessageStreamUncheckedUpdateInput = {
    messageId?: StringFieldUpdateOperationsInput | string
    streamName?: StringFieldUpdateOperationsInput | string
    position?: BigIntFieldUpdateOperationsInput | bigint | number
  }

  export type MessageStreamCreateManyInput = {
    messageId: string
    streamName: string
    position: bigint | number
  }

  export type MessageStreamUpdateManyMutationInput = {
    streamName?: StringFieldUpdateOperationsInput | string
    position?: BigIntFieldUpdateOperationsInput | bigint | number
  }

  export type MessageStreamUncheckedUpdateManyInput = {
    messageId?: StringFieldUpdateOperationsInput | string
    streamName?: StringFieldUpdateOperationsInput | string
    position?: BigIntFieldUpdateOperationsInput | bigint | number
  }

  export type StringFilter = {
    equals?: string
    in?: Enumerable<string>
    notIn?: Enumerable<string>
    lt?: string
    lte?: string
    gt?: string
    gte?: string
    contains?: string
    startsWith?: string
    endsWith?: string
    mode?: QueryMode
    not?: NestedStringFilter | string
  }

  export type BigIntFilter = {
    equals?: bigint | number
    in?: Enumerable<bigint> | Enumerable<number>
    notIn?: Enumerable<bigint> | Enumerable<number>
    lt?: bigint | number
    lte?: bigint | number
    gt?: bigint | number
    gte?: bigint | number
    not?: NestedBigIntFilter | bigint | number
  }
  export type JsonNullableFilter = 
    | PatchUndefined<
        Either<Required<JsonNullableFilterBase>, Exclude<keyof Required<JsonNullableFilterBase>, 'path'>>,
        Required<JsonNullableFilterBase>
      >
    | OptionalFlat<Omit<Required<JsonNullableFilterBase>, 'path'>>

  export type JsonNullableFilterBase = {
    equals?: InputJsonValue | null
    lt?: InputJsonValue
    lte?: InputJsonValue
    gt?: InputJsonValue
    gte?: InputJsonValue
    path?: Enumerable<string>
    string_contains?: string
    string_starts_with?: string
    string_ends_with?: string
    array_contains?: InputJsonValue
    array_starts_with?: InputJsonValue
    array_ends_with?: InputJsonValue
    not?: InputJsonValue | null
  }

  export type DateTimeFilter = {
    equals?: Date | string
    in?: Enumerable<Date> | Enumerable<string>
    notIn?: Enumerable<Date> | Enumerable<string>
    lt?: Date | string
    lte?: Date | string
    gt?: Date | string
    gte?: Date | string
    not?: NestedDateTimeFilter | Date | string
  }

  export type MessageStreamListRelationFilter = {
    every?: MessageStreamWhereInput
    some?: MessageStreamWhereInput
    none?: MessageStreamWhereInput
  }

  export type StringWithAggregatesFilter = {
    equals?: string
    in?: Enumerable<string>
    notIn?: Enumerable<string>
    lt?: string
    lte?: string
    gt?: string
    gte?: string
    contains?: string
    startsWith?: string
    endsWith?: string
    mode?: QueryMode
    not?: NestedStringWithAggregatesFilter | string
    _count?: NestedIntFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    count?: NestedIntFilter
    _min?: NestedStringFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    min?: NestedStringFilter
    _max?: NestedStringFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    max?: NestedStringFilter
  }

  export type BigIntWithAggregatesFilter = {
    equals?: bigint | number
    in?: Enumerable<bigint> | Enumerable<number>
    notIn?: Enumerable<bigint> | Enumerable<number>
    lt?: bigint | number
    lte?: bigint | number
    gt?: bigint | number
    gte?: bigint | number
    not?: NestedBigIntWithAggregatesFilter | bigint | number
    _count?: NestedIntFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    count?: NestedIntFilter
    _avg?: NestedFloatFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    avg?: NestedFloatFilter
    _sum?: NestedBigIntFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    sum?: NestedBigIntFilter
    _min?: NestedBigIntFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    min?: NestedBigIntFilter
    _max?: NestedBigIntFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    max?: NestedBigIntFilter
  }
  export type JsonNullableWithAggregatesFilter = 
    | PatchUndefined<
        Either<Required<JsonNullableWithAggregatesFilterBase>, Exclude<keyof Required<JsonNullableWithAggregatesFilterBase>, 'path'>>,
        Required<JsonNullableWithAggregatesFilterBase>
      >
    | OptionalFlat<Omit<Required<JsonNullableWithAggregatesFilterBase>, 'path'>>

  export type JsonNullableWithAggregatesFilterBase = {
    equals?: InputJsonValue | null
    lt?: InputJsonValue
    lte?: InputJsonValue
    gt?: InputJsonValue
    gte?: InputJsonValue
    path?: Enumerable<string>
    string_contains?: string
    string_starts_with?: string
    string_ends_with?: string
    array_contains?: InputJsonValue
    array_starts_with?: InputJsonValue
    array_ends_with?: InputJsonValue
    not?: InputJsonValue | null
    _count?: NestedIntNullableFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    count?: NestedIntNullableFilter
    _min?: NestedJsonNullableFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    min?: NestedJsonNullableFilter
    _max?: NestedJsonNullableFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    max?: NestedJsonNullableFilter
  }

  export type DateTimeWithAggregatesFilter = {
    equals?: Date | string
    in?: Enumerable<Date> | Enumerable<string>
    notIn?: Enumerable<Date> | Enumerable<string>
    lt?: Date | string
    lte?: Date | string
    gt?: Date | string
    gte?: Date | string
    not?: NestedDateTimeWithAggregatesFilter | Date | string
    _count?: NestedIntFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    count?: NestedIntFilter
    _min?: NestedDateTimeFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    min?: NestedDateTimeFilter
    _max?: NestedDateTimeFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    max?: NestedDateTimeFilter
  }

  export type MessageRelationFilter = {
    is?: MessageWhereInput
    isNot?: MessageWhereInput
  }

  export type MessageStreamMessages_stream_positionCompoundUniqueInput = {
    streamName: string
    position: bigint | number
  }

  export type MessageStreamMessageIdStreamNameCompoundUniqueInput = {
    messageId: string
    streamName: string
  }

  export type MessageStreamCreateNestedManyWithoutMessageInput = {
    create?: XOR<Enumerable<MessageStreamCreateWithoutMessageInput>, Enumerable<MessageStreamUncheckedCreateWithoutMessageInput>>
    connectOrCreate?: Enumerable<MessageStreamCreateOrConnectWithoutMessageInput>
    createMany?: MessageStreamCreateManyMessageInputEnvelope
    connect?: Enumerable<MessageStreamWhereUniqueInput>
  }

  export type MessageStreamUncheckedCreateNestedManyWithoutMessageInput = {
    create?: XOR<Enumerable<MessageStreamCreateWithoutMessageInput>, Enumerable<MessageStreamUncheckedCreateWithoutMessageInput>>
    connectOrCreate?: Enumerable<MessageStreamCreateOrConnectWithoutMessageInput>
    createMany?: MessageStreamCreateManyMessageInputEnvelope
    connect?: Enumerable<MessageStreamWhereUniqueInput>
  }

  export type StringFieldUpdateOperationsInput = {
    set?: string
  }

  export type BigIntFieldUpdateOperationsInput = {
    set?: bigint | number
    increment?: bigint | number
    decrement?: bigint | number
    multiply?: bigint | number
    divide?: bigint | number
  }

  export type DateTimeFieldUpdateOperationsInput = {
    set?: Date | string
  }

  export type MessageStreamUpdateManyWithoutMessageInput = {
    create?: XOR<Enumerable<MessageStreamCreateWithoutMessageInput>, Enumerable<MessageStreamUncheckedCreateWithoutMessageInput>>
    connectOrCreate?: Enumerable<MessageStreamCreateOrConnectWithoutMessageInput>
    upsert?: Enumerable<MessageStreamUpsertWithWhereUniqueWithoutMessageInput>
    createMany?: MessageStreamCreateManyMessageInputEnvelope
    connect?: Enumerable<MessageStreamWhereUniqueInput>
    set?: Enumerable<MessageStreamWhereUniqueInput>
    disconnect?: Enumerable<MessageStreamWhereUniqueInput>
    delete?: Enumerable<MessageStreamWhereUniqueInput>
    update?: Enumerable<MessageStreamUpdateWithWhereUniqueWithoutMessageInput>
    updateMany?: Enumerable<MessageStreamUpdateManyWithWhereWithoutMessageInput>
    deleteMany?: Enumerable<MessageStreamScalarWhereInput>
  }

  export type MessageStreamUncheckedUpdateManyWithoutMessageInput = {
    create?: XOR<Enumerable<MessageStreamCreateWithoutMessageInput>, Enumerable<MessageStreamUncheckedCreateWithoutMessageInput>>
    connectOrCreate?: Enumerable<MessageStreamCreateOrConnectWithoutMessageInput>
    upsert?: Enumerable<MessageStreamUpsertWithWhereUniqueWithoutMessageInput>
    createMany?: MessageStreamCreateManyMessageInputEnvelope
    connect?: Enumerable<MessageStreamWhereUniqueInput>
    set?: Enumerable<MessageStreamWhereUniqueInput>
    disconnect?: Enumerable<MessageStreamWhereUniqueInput>
    delete?: Enumerable<MessageStreamWhereUniqueInput>
    update?: Enumerable<MessageStreamUpdateWithWhereUniqueWithoutMessageInput>
    updateMany?: Enumerable<MessageStreamUpdateManyWithWhereWithoutMessageInput>
    deleteMany?: Enumerable<MessageStreamScalarWhereInput>
  }

  export type MessageCreateNestedOneWithoutStreamsInput = {
    create?: XOR<MessageCreateWithoutStreamsInput, MessageUncheckedCreateWithoutStreamsInput>
    connectOrCreate?: MessageCreateOrConnectWithoutStreamsInput
    connect?: MessageWhereUniqueInput
  }

  export type MessageUpdateOneRequiredWithoutStreamsInput = {
    create?: XOR<MessageCreateWithoutStreamsInput, MessageUncheckedCreateWithoutStreamsInput>
    connectOrCreate?: MessageCreateOrConnectWithoutStreamsInput
    upsert?: MessageUpsertWithoutStreamsInput
    connect?: MessageWhereUniqueInput
    update?: XOR<MessageUpdateWithoutStreamsInput, MessageUncheckedUpdateWithoutStreamsInput>
  }

  export type NestedStringFilter = {
    equals?: string
    in?: Enumerable<string>
    notIn?: Enumerable<string>
    lt?: string
    lte?: string
    gt?: string
    gte?: string
    contains?: string
    startsWith?: string
    endsWith?: string
    not?: NestedStringFilter | string
  }

  export type NestedBigIntFilter = {
    equals?: bigint | number
    in?: Enumerable<bigint> | Enumerable<number>
    notIn?: Enumerable<bigint> | Enumerable<number>
    lt?: bigint | number
    lte?: bigint | number
    gt?: bigint | number
    gte?: bigint | number
    not?: NestedBigIntFilter | bigint | number
  }

  export type NestedDateTimeFilter = {
    equals?: Date | string
    in?: Enumerable<Date> | Enumerable<string>
    notIn?: Enumerable<Date> | Enumerable<string>
    lt?: Date | string
    lte?: Date | string
    gt?: Date | string
    gte?: Date | string
    not?: NestedDateTimeFilter | Date | string
  }

  export type NestedStringWithAggregatesFilter = {
    equals?: string
    in?: Enumerable<string>
    notIn?: Enumerable<string>
    lt?: string
    lte?: string
    gt?: string
    gte?: string
    contains?: string
    startsWith?: string
    endsWith?: string
    not?: NestedStringWithAggregatesFilter | string
    _count?: NestedIntFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    count?: NestedIntFilter
    _min?: NestedStringFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    min?: NestedStringFilter
    _max?: NestedStringFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    max?: NestedStringFilter
  }

  export type NestedIntFilter = {
    equals?: number
    in?: Enumerable<number>
    notIn?: Enumerable<number>
    lt?: number
    lte?: number
    gt?: number
    gte?: number
    not?: NestedIntFilter | number
  }

  export type NestedBigIntWithAggregatesFilter = {
    equals?: bigint | number
    in?: Enumerable<bigint> | Enumerable<number>
    notIn?: Enumerable<bigint> | Enumerable<number>
    lt?: bigint | number
    lte?: bigint | number
    gt?: bigint | number
    gte?: bigint | number
    not?: NestedBigIntWithAggregatesFilter | bigint | number
    _count?: NestedIntFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    count?: NestedIntFilter
    _avg?: NestedFloatFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    avg?: NestedFloatFilter
    _sum?: NestedBigIntFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    sum?: NestedBigIntFilter
    _min?: NestedBigIntFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    min?: NestedBigIntFilter
    _max?: NestedBigIntFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    max?: NestedBigIntFilter
  }

  export type NestedFloatFilter = {
    equals?: number
    in?: Enumerable<number>
    notIn?: Enumerable<number>
    lt?: number
    lte?: number
    gt?: number
    gte?: number
    not?: NestedFloatFilter | number
  }

  export type NestedIntNullableFilter = {
    equals?: number | null
    in?: Enumerable<number> | null
    notIn?: Enumerable<number> | null
    lt?: number
    lte?: number
    gt?: number
    gte?: number
    not?: NestedIntNullableFilter | number | null
  }
  export type NestedJsonNullableFilter = 
    | PatchUndefined<
        Either<Required<NestedJsonNullableFilterBase>, Exclude<keyof Required<NestedJsonNullableFilterBase>, 'path'>>,
        Required<NestedJsonNullableFilterBase>
      >
    | OptionalFlat<Omit<Required<NestedJsonNullableFilterBase>, 'path'>>

  export type NestedJsonNullableFilterBase = {
    equals?: InputJsonValue | null
    lt?: InputJsonValue
    lte?: InputJsonValue
    gt?: InputJsonValue
    gte?: InputJsonValue
    path?: Enumerable<string>
    string_contains?: string
    string_starts_with?: string
    string_ends_with?: string
    array_contains?: InputJsonValue
    array_starts_with?: InputJsonValue
    array_ends_with?: InputJsonValue
    not?: InputJsonValue | null
  }

  export type NestedDateTimeWithAggregatesFilter = {
    equals?: Date | string
    in?: Enumerable<Date> | Enumerable<string>
    notIn?: Enumerable<Date> | Enumerable<string>
    lt?: Date | string
    lte?: Date | string
    gt?: Date | string
    gte?: Date | string
    not?: NestedDateTimeWithAggregatesFilter | Date | string
    _count?: NestedIntFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    count?: NestedIntFilter
    _min?: NestedDateTimeFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    min?: NestedDateTimeFilter
    _max?: NestedDateTimeFilter
    /**
     * @deprecated since 2.23 because Aggregation keywords got unified to use underscore as prefix to prevent field clashes.
     * 
    **/
    max?: NestedDateTimeFilter
  }

  export type MessageStreamCreateWithoutMessageInput = {
    streamName: string
    position: bigint | number
  }

  export type MessageStreamUncheckedCreateWithoutMessageInput = {
    streamName: string
    position: bigint | number
  }

  export type MessageStreamCreateOrConnectWithoutMessageInput = {
    where: MessageStreamWhereUniqueInput
    create: XOR<MessageStreamCreateWithoutMessageInput, MessageStreamUncheckedCreateWithoutMessageInput>
  }

  export type MessageStreamCreateManyMessageInputEnvelope = {
    data: Enumerable<MessageStreamCreateManyMessageInput>
    skipDuplicates?: boolean
  }

  export type MessageStreamUpsertWithWhereUniqueWithoutMessageInput = {
    where: MessageStreamWhereUniqueInput
    update: XOR<MessageStreamUpdateWithoutMessageInput, MessageStreamUncheckedUpdateWithoutMessageInput>
    create: XOR<MessageStreamCreateWithoutMessageInput, MessageStreamUncheckedCreateWithoutMessageInput>
  }

  export type MessageStreamUpdateWithWhereUniqueWithoutMessageInput = {
    where: MessageStreamWhereUniqueInput
    data: XOR<MessageStreamUpdateWithoutMessageInput, MessageStreamUncheckedUpdateWithoutMessageInput>
  }

  export type MessageStreamUpdateManyWithWhereWithoutMessageInput = {
    where: MessageStreamScalarWhereInput
    data: XOR<MessageStreamUpdateManyMutationInput, MessageStreamUncheckedUpdateManyWithoutStreamsInput>
  }

  export type MessageStreamScalarWhereInput = {
    AND?: Enumerable<MessageStreamScalarWhereInput>
    OR?: Enumerable<MessageStreamScalarWhereInput>
    NOT?: Enumerable<MessageStreamScalarWhereInput>
    messageId?: StringFilter | string
    streamName?: StringFilter | string
    position?: BigIntFilter | bigint | number
  }

  export type MessageCreateWithoutStreamsInput = {
    id?: string
    type: string
    globalPosition?: bigint | number
    data?: InputJsonValue | null
    metadata?: InputJsonValue | null
    time?: Date | string
  }

  export type MessageUncheckedCreateWithoutStreamsInput = {
    id?: string
    type: string
    globalPosition?: bigint | number
    data?: InputJsonValue | null
    metadata?: InputJsonValue | null
    time?: Date | string
  }

  export type MessageCreateOrConnectWithoutStreamsInput = {
    where: MessageWhereUniqueInput
    create: XOR<MessageCreateWithoutStreamsInput, MessageUncheckedCreateWithoutStreamsInput>
  }

  export type MessageUpsertWithoutStreamsInput = {
    update: XOR<MessageUpdateWithoutStreamsInput, MessageUncheckedUpdateWithoutStreamsInput>
    create: XOR<MessageCreateWithoutStreamsInput, MessageUncheckedCreateWithoutStreamsInput>
  }

  export type MessageUpdateWithoutStreamsInput = {
    id?: StringFieldUpdateOperationsInput | string
    type?: StringFieldUpdateOperationsInput | string
    globalPosition?: BigIntFieldUpdateOperationsInput | bigint | number
    data?: InputJsonValue | null
    metadata?: InputJsonValue | null
    time?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type MessageUncheckedUpdateWithoutStreamsInput = {
    id?: StringFieldUpdateOperationsInput | string
    type?: StringFieldUpdateOperationsInput | string
    globalPosition?: BigIntFieldUpdateOperationsInput | bigint | number
    data?: InputJsonValue | null
    metadata?: InputJsonValue | null
    time?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type MessageStreamCreateManyMessageInput = {
    streamName: string
    position: bigint | number
  }

  export type MessageStreamUpdateWithoutMessageInput = {
    streamName?: StringFieldUpdateOperationsInput | string
    position?: BigIntFieldUpdateOperationsInput | bigint | number
  }

  export type MessageStreamUncheckedUpdateWithoutMessageInput = {
    streamName?: StringFieldUpdateOperationsInput | string
    position?: BigIntFieldUpdateOperationsInput | bigint | number
  }

  export type MessageStreamUncheckedUpdateManyWithoutStreamsInput = {
    streamName?: StringFieldUpdateOperationsInput | string
    position?: BigIntFieldUpdateOperationsInput | bigint | number
  }



  /**
   * Batch Payload for updateMany & deleteMany & createMany
   */

  export type BatchPayload = {
    count: number
  }

  /**
   * DMMF
   */
  export const dmmf: runtime.DMMF.Document;
}