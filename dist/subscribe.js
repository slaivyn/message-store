"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bluebird_1 = __importDefault(require("bluebird"));
const uuid_1 = require("uuid");
const category_1 = __importDefault(require("./category"));
function configureCreateSubscription({ readCategory, readLastMessage, write, logger, }) {
    const createSubscription = ({ streamName, handlers, subscriberId, messagesPerTick = 100, positionUpdateInterval = 100, originStreamName = null, tickIntervalMs = 100, }) => {
        const subscriberStreamName = `subscriberPosition-${subscriberId}`;
        let currentPosition = BigInt(0);
        let messagesSinceLastPositionWrite = 0;
        let keepGoing = true;
        function writePosition(position) {
            const positionEvent = {
                id: uuid_1.v4(),
                type: "Read",
                data: { position: position.toString() },
            };
            return write(subscriberStreamName, positionEvent);
        }
        function updateReadPosition(position) {
            currentPosition = position;
            messagesSinceLastPositionWrite += 1;
            if (messagesSinceLastPositionWrite === positionUpdateInterval) {
                messagesSinceLastPositionWrite = 0;
                return writePosition(position);
            }
            return bluebird_1.default.resolve();
        }
        function loadPosition() {
            return readLastMessage(subscriberStreamName).then((message) => {
                currentPosition = BigInt((message === null || message === void 0 ? void 0 : message.data) && typeof message.data === "object"
                    ? message.data.position
                    : 0);
            });
        }
        function filterOnOriginMatch(messages) {
            if (!originStreamName) {
                return messages;
            }
            return messages.filter((message) => {
                if (!message.metadata || typeof message.metadata !== "object")
                    return false;
                const originCategory = category_1.default(message.metadata.originStreamName);
                return originStreamName === originCategory;
            });
        }
        function getNextBatchOfMessages() {
            return readCategory(streamName, currentPosition + BigInt(1), messagesPerTick).then(filterOnOriginMatch);
        }
        function handleMessage(message) {
            const handler = handlers[message.type]; //|| handlers.$any;
            return handler ? handler(message) : Promise.resolve(true);
        }
        function processBatch(messages) {
            return bluebird_1.default.each(messages, (message) => handleMessage(message)
                .then(() => updateReadPosition(message.globalPosition))
                .catch((err) => {
                logError(message, err);
                // Re-throw so that we can break the chain
                throw err;
            })).then(() => messages.length);
        }
        function logError(lastMessage, error) {
            // eslint-disable-next-line no-console
            logger.error(`error processing message (${subscriberId}-${lastMessage.id}): ${error}`);
        }
        /**
         * @description - Generally not called from the outside.  This function is
         *   called on each of the timeouts to see if there are new events that need
         *   processing.
         */
        function tick() {
            return getNextBatchOfMessages()
                .then(processBatch)
                .catch((err) => {
                // eslint-disable-next-line no-console
                logger.error("Error processing batch", err);
                stop();
            });
        }
        function poll() {
            return __awaiter(this, void 0, void 0, function* () {
                yield loadPosition();
                // eslint-disable-next-line no-unmodified-loop-condition
                while (keepGoing) {
                    const messagesProcessed = yield tick();
                    if (messagesProcessed === 0) {
                        yield bluebird_1.default.delay(tickIntervalMs);
                    }
                }
            });
        }
        function start() {
            // eslint-disable-next-line
            console.log(`Started ${subscriberId}`);
            return poll();
        }
        function stop() {
            // eslint-disable-next-line
            console.log(`Stopped ${subscriberId}`);
            keepGoing = false;
        }
        return {
            loadPosition,
            start,
            stop,
            // tick,
            writePosition,
        };
    };
    return createSubscription;
}
exports.default = configureCreateSubscription;
