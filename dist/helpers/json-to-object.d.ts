import { Prisma } from "../../prisma-client";
declare function jsonToObject(input: Prisma.JsonValue): Prisma.JsonObject;
export default jsonToObject;
