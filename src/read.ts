import { PrismaClient } from "prisma-client";
import { PrismaClientUnknownRequestError } from "@prisma/client/runtime";
import winston from "winston";

import getAllMessages from "./read/get-all-messages";
import getCategoryMessages from "./read/get-category-messages";
import getLastStreamMessage from "./read/get-last-stream-message";
import getStreamMessages from "./read/get-stream-messages";
import {
  Projection,
  FetchFunction,
  ReadLastMessageFunction,
  ReadFunction,
  ReadCategoryFunction,
  ReadStreamFunction,
  SingleStreamMessage,
  InnerMessage,
} from "./types";

// const getLastMessageSql = "SELECT * FROM get_last_stream_message($1)";
// const getCategoryMessagesSql =
//   "SELECT * FROM get_category_messages($1, $2, $3)";
// const getStreamMessagesSql = "SELECT * FROM get_stream_messages($1, $2, $3)";
// const getAllMessagesSql = `
//   SELECT
//     id::varchar,
//     stream_name::varchar,
//     type::varchar,
//     position::bigint,
//     global_position::bigint,
//     data::varchar,
//     metadata::varchar,
//     time::timestamp
//   FROM
//     messages
//   WHERE
//     global_position > $1
//   LIMIT $2`;

/**
 * @description Projects an array of events, running them through projection.
 * Does so by checking to see if `projection` defines a handler for each event
 * type.  If it does, then it calls into that function with the running state
 * and current event, passing the return value to the next iteration.
 */
export function project<Events extends InnerMessage, Entity>(
  events: SingleStreamMessage<Events>[],
  projection: Projection<Events, Entity>
) {
  return events.reduce((entity, event) => {
    if (!projection[event.type]) {
      return entity;
    }

    return projection[event.type](entity, event);
  }, projection.$init());
}

export default function createRead({
  prisma,
  logger,
}: {
  prisma: PrismaClient;
  logger: winston.Logger;
}) {
  /**
   * @description Fetches an entity from the store by loading the messages for
   */
  const fetch: FetchFunction = function (streamName, projection) {
    type ExtractedEvents = typeof projection extends Projection<infer X>
      ? X
      : never;
    return readStream<ExtractedEvents>(streamName)
      .then((messages) => project(messages, projection))
      .catch((err) => {
        logger.error(err);
        throw err;
      });
  };

  /**
   * @description Dispatches a read request to the correct reader function.
   * Which function to use is determined by the `streamName` parameter.  This
   * function supports category streams and the special `$all` stream and reads
   * `maxCount` messages starting from `fromPosition` within the stream.  `$all`
   * and category streams use the `id` column, and all other streams use
   * `position`.
   */
  const read: ReadFunction = (
    streamName: string,
    fromPosition = BigInt(0),
    maxMessages = 1000
  ) => {
    if (streamName === "$all") {
      return getAllMessages(prisma, fromPosition, maxMessages);
    }
    if (streamName.includes("-"))
      return readStream(streamName, fromPosition, maxMessages);

    return readCategory(streamName, fromPosition, maxMessages);
  };

  const readCategory: ReadCategoryFunction = (
    streamName: string,
    fromPosition = BigInt(0),
    maxMessages = 1000
  ) => {
    if (streamName.includes("-"))
      throw new Error('Category stream must not contain "-"');
    return getCategoryMessages(
      prisma,
      streamName,
      fromPosition,
      maxMessages
    ).catch((e) => {
      logger.error(e);
      if (
        process.env.NODE_ENV !== "production" &&
        e instanceof PrismaClientUnknownRequestError
      ) {
        return [];
      }
      console.error("getCategoryMessages error", e);
      throw e;
    });
  };

  const readStream: ReadStreamFunction = (
    streamName: string,
    fromPosition = BigInt(0),
    maxMessages = 1000
  ) => {
    if (!streamName.includes("-"))
      throw new Error('Message stream must contain "-"');
    return getStreamMessages(
      prisma,
      streamName,
      fromPosition,
      maxMessages
    ).catch((e) => {
      logger.error(e);
      if (
        process.env.NODE_ENV !== "production" &&
        e instanceof PrismaClientUnknownRequestError
      ) {
        return [];
      }
      console.error("getStreamMessages error", e);
      throw e;
    });
  };

  /**
   * @description Returns the most recently written message in an entity stream.
   * This does not work on categories.
   */
  const readLastMessage: ReadLastMessageFunction = (streamName: string) => {
    return getLastStreamMessage(prisma, streamName).catch((e) => {
      logger.error(e);
      if (
        process.env.NODE_ENV !== "production" &&
        e instanceof PrismaClientUnknownRequestError
      ) {
        return null;
      }
      console.error("getLastStreamMessage error", e);
      throw e;
    });
  };

  return {
    read,
    readStream,
    readCategory,
    readLastMessage,
    fetch,
  };
}
