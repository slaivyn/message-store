"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const VersionConflictError = function (streamName, expected, actual) {
    Error.captureStackTrace(this, this.constructor);
    const message = [
        "VersionConflict: stream",
        streamName,
        "expected version",
        expected,
        "but was at version",
        actual,
    ].join(" ");
    this.message = message;
    this.name = "VersionConflictError";
};
VersionConflictError.prototype = Object.create(Error.prototype);
VersionConflictError.prototype.constructor = VersionConflictError;
exports.default = VersionConflictError;
