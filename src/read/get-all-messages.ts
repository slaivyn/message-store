import { PrismaClient } from "prisma-client";
import jsonToObject from "../helpers/json-to-object";
import { MultiStreamMessage } from "../types";

function getAllMessages(
  prisma: PrismaClient,
  fromPosition = BigInt(0),
  maxMessages = 1000
): Promise<MultiStreamMessage[]> {
  return prisma.message
    .findMany({
      where: { globalPosition: { gte: fromPosition } },
      take: maxMessages,
      include: {
        streams: {
          select: {
            position: true,
            streamName: true,
          },
        },
      },
    })
    .then((messages) =>
      messages.map((message) => ({
        ...message,
        data: jsonToObject(message.data),
        metadata: jsonToObject(message.metadata),
      }))
    );
}
export default getAllMessages;
