"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("uuid");
const version_conflict_error_1 = __importDefault(require("./version-conflict-error"));
const versionConflictErrorRegex = /^Wrong.*Stream: (\w+), Stream Version: (\d+)\)/;
function createWrite({ prisma, logger, }) {
    try {
        return function write(stream, message) {
            if (!message.type) {
                throw new Error("Messages must have a type");
            }
            const streams = typeof stream === "string"
                ? [[stream, null]]
                : !Array.isArray(stream)
                    ? [[stream.name, stream.expectedVersion || null]]
                    : stream.map((s) => typeof s === "string"
                        ? [s, null]
                        : [s.name, s.expectedVersion || null]);
            const uniqueStreams = streams.reduce((acc, [name, version]) => {
                const idx = acc.findIndex((s) => s[0] === name);
                const existing = acc[idx];
                if (!existing)
                    return [...acc, [name, version]];
                if (version && existing[1] && version < existing[1])
                    return [
                        ...acc.slice(0, idx),
                        [name, version],
                        ...acc.slice(idx + 1),
                    ];
                return acc;
            }, []);
            const id = message.id || uuid_1.v4();
            const data = message.data || {};
            const metadata = message.metadata || {};
            const writeFunctionSql = `SELECT write_message(
      '${id}'::varchar,
      ARRAY[${uniqueStreams.map((s) => `ROW('${s[0]}', ${s[1]})`)}]::Stream[],
      '${message.type}'::varchar,
      '${JSON.stringify(data)}'::jsonb,
      '${JSON.stringify(metadata)}'::jsonb
    )`;
            console.log(writeFunctionSql, uniqueStreams);
            return prisma
                .$queryRaw(writeFunctionSql)
                .catch((err) => {
                var _a;
                console.log("err write", err);
                const errorMatch = err.message.match(versionConflictErrorRegex);
                if (errorMatch === null || uniqueStreams.every((s) => s[1] == null)) {
                    throw err;
                }
                const streamName = errorMatch[1];
                const actualVersion = parseInt(errorMatch[2], 10);
                const expectedVersion = (_a = uniqueStreams.find((s) => s[0] === streamName)) === null || _a === void 0 ? void 0 : _a[1];
                const versionConflictError = new version_conflict_error_1.default(streamName, actualVersion, expectedVersion);
                versionConflictError.stack = err.stack;
                throw versionConflictError;
            });
        };
    }
    catch (err) {
        logger.error(err);
        throw err;
    }
}
exports.default = createWrite;
module.exports = createWrite;
