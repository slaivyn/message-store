import { PrismaClient } from "prisma-client";
import jsonToObject from "../helpers/json-to-object";
import { SingleStreamMessage } from "../types";

function getLastStreamMessage(
  prisma: PrismaClient,
  streamName: string
): Promise<SingleStreamMessage> {
  if (!streamName.includes("-")) throw `Must be a stream name: ${streamName}`;
  return prisma.messageStream
    .findMany({
      where: {
        streamName,
      },
      take: 1,
      orderBy: { position: "desc" },
      include: {
        message: true,
      },
    })
    .then((messages) =>
      messages.map(({ streamName, position, message }) => ({
        ...message,
        data: jsonToObject(message.data),
        metadata: jsonToObject(message.metadata),
        streamName,
        position,
      }))
    )
    .then((messages) => messages[0]);
}
export default getLastStreamMessage;
