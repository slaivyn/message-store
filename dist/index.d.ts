import { Prisma, PrismaClient } from "../prisma-client";
import winston from "winston";
export { default as VersionConflictError } from "./version-conflict-error";
export { project } from "./read";
export default function createMessageStore({ datasource, logger: providedLogger, }: {
    datasource: Prisma.Datasource;
    logger?: winston.Logger;
}): {
    prisma: PrismaClient<Prisma.PrismaClientOptions, never, boolean | ((error: Error) => Error) | Prisma.RejectPerOperation | undefined>;
    write: import("./types").WriteFunction;
    createSubscription: import("./types").CreateSubscriptionFunction;
    read: import("./types").ReadFunction;
    readStream: import("./types").ReadStreamFunction;
    readCategory: import("./types").ReadCategoryFunction;
    readLastMessage: import("./types").ReadLastMessageFunction;
    fetch: import("./types").FetchFunction;
    stop: () => Promise<any>;
};
declare type MessageStore = ReturnType<typeof createMessageStore>;
export type { MessageStore };
