import { Prisma } from "prisma-client";

function jsonToObject(input: Prisma.JsonValue) {
  if (input && typeof input === "object" && !Array.isArray(input)) return input;
  return {} as Prisma.JsonObject;
}
export default jsonToObject;
