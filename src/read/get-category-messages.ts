import { PrismaClient } from "prisma-client";
import jsonToObject from "../helpers/json-to-object";
import { MultiStreamMessage } from "../types";

function getCategoryMessages(
  prisma: PrismaClient,
  streamName: string,
  fromPosition = BigInt(1),
  maxMessages = 1000
): Promise<MultiStreamMessage[]> {
  const categoryName = streamName.split("-")[0];
  return prisma.message
    .findMany({
      where: {
        globalPosition: { gte: fromPosition },
        streams: {
          some: {
            streamName: { startsWith: `${categoryName}-` },
          },
        },
      },
      include: {
        streams: {
          select: {
            position: true,
            streamName: true,
          },
        },
      },
      take: maxMessages,
      orderBy: { globalPosition: "asc" },
    })
    .then((messages) =>
      messages.map((message) => ({
        ...message,
        data: jsonToObject(message.data),
        metadata: jsonToObject(message.metadata),
      }))
    );
  // return prisma.messageStream
  //   .findMany({
  //     where: {
  //       streamName: { startsWith: `${categoryName}-` },
  //       message: {
  //         globalPosition: { gte: fromPosition },
  //       },
  //     },
  //     take: maxMessages,
  //     orderBy: { position: "asc" },
  //     include: {
  //       message: true,
  //     },
  //   })
  //   .then((messages) => {
  //     const index: {[key: string]: any} = {}
  //     messages
  //       .map(({ streamName, position, message }) => ({
  //         ...message,
  //         data: jsonToObject(message.data),
  //         metadata: jsonToObject(message.metadata),
  //         streamName,
  //         position,
  //       }))
  //       .filter(({ id }) => {
  //         if (index[id]) return false
  //         index[id] = id
  //         return true
  //       })
  //   });
}
export default getCategoryMessages;
