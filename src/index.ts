import { Prisma, PrismaClient } from "prisma-client";
import createRead from "./read";
import configureCreateSubscription from "./subscribe";
import createWrite from "./write";
import winston from "winston";

export { default as VersionConflictError } from "./version-conflict-error";
export { project } from "./read";

export default function createMessageStore({
  datasource,
  logger: providedLogger,
}: {
  datasource: Prisma.Datasource;
  logger?: winston.Logger;
}) {
  const logger =
    providedLogger ||
    winston.createLogger({
      transports: [
        new winston.transports.Console({
          level: "info",
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.simple()
          ),
        }),
      ],
    });
  const prisma = new PrismaClient({
    datasources: { db: datasource },
    __internal: {
      useUds: true,
    },
  } as any);
  const stop = () => {
    console.log("stop message-store prisma");
    return prisma.$disconnect();
  };
  prisma.$on("beforeExit", () => {
    console.log("beforeExit");
  });
  const write = createWrite({ prisma, logger });
  const { read, readStream, readCategory, readLastMessage, fetch } = createRead(
    { prisma, logger }
  );
  const createSubscription = configureCreateSubscription({
    readCategory,
    readLastMessage,
    write,
    logger,
  });

  return {
    prisma,
    write,
    createSubscription,
    read,
    readStream,
    readCategory,
    readLastMessage,
    fetch,
    stop,
  };
}

type MessageStore = ReturnType<typeof createMessageStore>;
export type { MessageStore };
