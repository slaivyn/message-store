import winston from "winston";
import { CreateSubscriptionFunction, ReadCategoryFunction, ReadLastMessageFunction, WriteFunction } from "./types";
interface IConfigureCreateSubscription {
    readCategory: ReadCategoryFunction;
    readLastMessage: ReadLastMessageFunction;
    write: WriteFunction;
    logger: winston.Logger;
}
export default function configureCreateSubscription({ readCategory, readLastMessage, write, logger, }: IConfigureCreateSubscription): CreateSubscriptionFunction;
export {};
