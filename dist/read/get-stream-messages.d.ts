import { PrismaClient } from "../../prisma-client";
import { InnerMessage, SingleStreamMessage } from "../types";
declare function getStreamMessages<Events extends InnerMessage>(prisma: PrismaClient, streamName: string, fromPosition?: bigint, maxMessages?: number): Promise<SingleStreamMessage<Events>[]>;
export default getStreamMessages;
