"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const json_to_object_1 = __importDefault(require("../helpers/json-to-object"));
function getCategoryMessages(prisma, streamName, fromPosition = BigInt(1), maxMessages = 1000) {
    const categoryName = streamName.split("-")[0];
    return prisma.message
        .findMany({
        where: {
            globalPosition: { gte: fromPosition },
            streams: {
                some: {
                    streamName: { startsWith: `${categoryName}-` },
                },
            },
        },
        include: {
            streams: {
                select: {
                    position: true,
                    streamName: true,
                },
            },
        },
        take: maxMessages,
        orderBy: { globalPosition: "asc" },
    })
        .then((messages) => messages.map((message) => (Object.assign(Object.assign({}, message), { data: json_to_object_1.default(message.data), metadata: json_to_object_1.default(message.metadata) }))));
    // return prisma.messageStream
    //   .findMany({
    //     where: {
    //       streamName: { startsWith: `${categoryName}-` },
    //       message: {
    //         globalPosition: { gte: fromPosition },
    //       },
    //     },
    //     take: maxMessages,
    //     orderBy: { position: "asc" },
    //     include: {
    //       message: true,
    //     },
    //   })
    //   .then((messages) => {
    //     const index: {[key: string]: any} = {}
    //     messages
    //       .map(({ streamName, position, message }) => ({
    //         ...message,
    //         data: jsonToObject(message.data),
    //         metadata: jsonToObject(message.metadata),
    //         streamName,
    //         position,
    //       }))
    //       .filter(({ id }) => {
    //         if (index[id]) return false
    //         index[id] = id
    //         return true
    //       })
    //   });
}
exports.default = getCategoryMessages;
