"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.project = void 0;
const runtime_1 = require("@prisma/client/runtime");
const get_all_messages_1 = __importDefault(require("./read/get-all-messages"));
const get_category_messages_1 = __importDefault(require("./read/get-category-messages"));
const get_last_stream_message_1 = __importDefault(require("./read/get-last-stream-message"));
const get_stream_messages_1 = __importDefault(require("./read/get-stream-messages"));
// const getLastMessageSql = "SELECT * FROM get_last_stream_message($1)";
// const getCategoryMessagesSql =
//   "SELECT * FROM get_category_messages($1, $2, $3)";
// const getStreamMessagesSql = "SELECT * FROM get_stream_messages($1, $2, $3)";
// const getAllMessagesSql = `
//   SELECT
//     id::varchar,
//     stream_name::varchar,
//     type::varchar,
//     position::bigint,
//     global_position::bigint,
//     data::varchar,
//     metadata::varchar,
//     time::timestamp
//   FROM
//     messages
//   WHERE
//     global_position > $1
//   LIMIT $2`;
/**
 * @description Projects an array of events, running them through projection.
 * Does so by checking to see if `projection` defines a handler for each event
 * type.  If it does, then it calls into that function with the running state
 * and current event, passing the return value to the next iteration.
 */
function project(events, projection) {
    return events.reduce((entity, event) => {
        if (!projection[event.type]) {
            return entity;
        }
        return projection[event.type](entity, event);
    }, projection.$init());
}
exports.project = project;
function createRead({ prisma, logger, }) {
    /**
     * @description Fetches an entity from the store by loading the messages for
     */
    const fetch = function (streamName, projection) {
        return readStream(streamName)
            .then((messages) => project(messages, projection))
            .catch((err) => {
            logger.error(err);
            throw err;
        });
    };
    /**
     * @description Dispatches a read request to the correct reader function.
     * Which function to use is determined by the `streamName` parameter.  This
     * function supports category streams and the special `$all` stream and reads
     * `maxCount` messages starting from `fromPosition` within the stream.  `$all`
     * and category streams use the `id` column, and all other streams use
     * `position`.
     */
    const read = (streamName, fromPosition = BigInt(0), maxMessages = 1000) => {
        if (streamName === "$all") {
            return get_all_messages_1.default(prisma, fromPosition, maxMessages);
        }
        if (streamName.includes("-"))
            return readStream(streamName, fromPosition, maxMessages);
        return readCategory(streamName, fromPosition, maxMessages);
    };
    const readCategory = (streamName, fromPosition = BigInt(0), maxMessages = 1000) => {
        if (streamName.includes("-"))
            throw new Error('Category stream must not contain "-"');
        return get_category_messages_1.default(prisma, streamName, fromPosition, maxMessages).catch((e) => {
            logger.error(e);
            if (process.env.NODE_ENV !== "production" &&
                e instanceof runtime_1.PrismaClientUnknownRequestError) {
                return [];
            }
            console.error("getCategoryMessages error", e);
            throw e;
        });
    };
    const readStream = (streamName, fromPosition = BigInt(0), maxMessages = 1000) => {
        if (!streamName.includes("-"))
            throw new Error('Message stream must contain "-"');
        return get_stream_messages_1.default(prisma, streamName, fromPosition, maxMessages).catch((e) => {
            logger.error(e);
            if (process.env.NODE_ENV !== "production" &&
                e instanceof runtime_1.PrismaClientUnknownRequestError) {
                return [];
            }
            console.error("getStreamMessages error", e);
            throw e;
        });
    };
    /**
     * @description Returns the most recently written message in an entity stream.
     * This does not work on categories.
     */
    const readLastMessage = (streamName) => {
        return get_last_stream_message_1.default(prisma, streamName).catch((e) => {
            logger.error(e);
            if (process.env.NODE_ENV !== "production" &&
                e instanceof runtime_1.PrismaClientUnknownRequestError) {
                return null;
            }
            console.error("getLastStreamMessage error", e);
            throw e;
        });
    };
    return {
        read,
        readStream,
        readCategory,
        readLastMessage,
        fetch,
    };
}
exports.default = createRead;
