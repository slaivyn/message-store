"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const json_to_object_1 = __importDefault(require("../helpers/json-to-object"));
function getAllMessages(prisma, fromPosition = BigInt(0), maxMessages = 1000) {
    return prisma.message
        .findMany({
        where: { globalPosition: { gte: fromPosition } },
        take: maxMessages,
        include: {
            streams: {
                select: {
                    position: true,
                    streamName: true,
                },
            },
        },
    })
        .then((messages) => messages.map((message) => (Object.assign(Object.assign({}, message), { data: json_to_object_1.default(message.data), metadata: json_to_object_1.default(message.metadata) }))));
}
exports.default = getAllMessages;
