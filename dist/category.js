"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function category(streamName) {
    if (streamName == null) {
        return "";
    }
    return streamName.split("-")[0];
}
exports.default = category;
