import { PrismaClient } from "../prisma-client";
import winston from "winston";
import { WriteFunction } from "./types";
export default function createWrite({ prisma, logger, }: {
    prisma: PrismaClient;
    logger: winston.Logger;
}): WriteFunction;
