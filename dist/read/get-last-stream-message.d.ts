import { PrismaClient } from "../../prisma-client";
import { SingleStreamMessage } from "../types";
declare function getLastStreamMessage(prisma: PrismaClient, streamName: string): Promise<SingleStreamMessage>;
export default getLastStreamMessage;
