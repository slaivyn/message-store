import * as Sentry from "@sentry/node";
declare type SentryType = typeof Sentry;
declare const createLogger: ({ Sentry }: {
    Sentry?: typeof Sentry | undefined;
}) => {
    error: (exception: any) => void;
};
export declare type Logger = ReturnType<typeof createLogger>;
export default createLogger;
