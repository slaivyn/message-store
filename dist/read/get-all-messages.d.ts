import { PrismaClient } from "../../prisma-client";
import { MultiStreamMessage } from "../types";
declare function getAllMessages(prisma: PrismaClient, fromPosition?: bigint, maxMessages?: number): Promise<MultiStreamMessage[]>;
export default getAllMessages;
