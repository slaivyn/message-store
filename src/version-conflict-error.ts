const VersionConflictError = function (
  this: { message: string; name: string; constructor: typeof Error },
  streamName: string,
  expected: number,
  actual: number
) {
  Error.captureStackTrace(this, this.constructor);
  const message = [
    "VersionConflict: stream",
    streamName,
    "expected version",
    expected,
    "but was at version",
    actual,
  ].join(" ");

  this.message = message;
  this.name = "VersionConflictError";
} as any;

VersionConflictError.prototype = Object.create(Error.prototype);
VersionConflictError.prototype.constructor = VersionConflictError;
export default VersionConflictError;
