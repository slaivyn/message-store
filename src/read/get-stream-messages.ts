import { PrismaClient } from "prisma-client";
import jsonToObject from "../helpers/json-to-object";
import { InnerMessage, SingleStreamMessage } from "../types";

function getStreamMessages<Events extends InnerMessage>(
  prisma: PrismaClient,
  streamName: string,
  fromPosition = BigInt(0),
  maxMessages = 1000
): Promise<SingleStreamMessage<Events>[]> {
  if (!streamName.includes("-")) throw `Must be a stream name: ${streamName}`;
  return prisma.messageStream
    .findMany({
      where: {
        streamName,
        position: { gte: fromPosition },
      },
      take: maxMessages,
      orderBy: { position: "asc" },
      include: {
        message: true,
      },
    })
    .then((messages) =>
      messages.map(({ streamName, position, message }) => ({
        ...message,
        data: jsonToObject(message.data),
        metadata: jsonToObject(message.metadata),
        streamName,
        position,
      }))
    );
}
export default getStreamMessages;
