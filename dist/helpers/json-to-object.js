"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function jsonToObject(input) {
    if (input && typeof input === "object" && !Array.isArray(input))
        return input;
    return {};
}
exports.default = jsonToObject;
