-- CreateTable
CREATE TABLE "messages" (
    "id" UUID NOT NULL DEFAULT gen_random_uuid(),
    "type" TEXT NOT NULL,
    "global_position" BIGSERIAL NOT NULL,
    "data" JSONB,
    "metadata" JSONB,
    "time" TIMESTAMP(6) NOT NULL DEFAULT timezone('utc'::text, now()),

    PRIMARY KEY ("global_position")
);

-- CreateTable
CREATE TABLE "message_stream" (
    "message_id" UUID NOT NULL,
    "stream_name" TEXT NOT NULL,
    "position" BIGINT NOT NULL,

    PRIMARY KEY ("message_id","stream_name")
);

-- CreateIndex
CREATE UNIQUE INDEX "messages.id_unique" ON "messages"("id");

-- CreateIndex
CREATE INDEX "messages_category" ON "messages"("global_position");

-- CreateIndex
CREATE UNIQUE INDEX "messages_stream_position" ON "message_stream"("stream_name", "position");

-- AddForeignKey
ALTER TABLE "message_stream" ADD FOREIGN KEY ("message_id") REFERENCES "messages"("id") ON DELETE CASCADE ON UPDATE CASCADE;





DROP TYPE IF EXISTS stream CASCADE;
CREATE TYPE stream AS (
  stream_name varchar,
  expected_version bigint
);


CREATE OR REPLACE FUNCTION hash_64(
  value varchar
)
RETURNS bigint
AS $$
DECLARE
  _hash bigint;
BEGIN
  SELECT left('x' || md5(hash_64.value), 17)::bit(64)::bigint INTO _hash;
  return _hash;
END;
$$ LANGUAGE plpgsql
IMMUTABLE;

CREATE OR REPLACE FUNCTION acquire_lock(
  stream_name varchar
)
RETURNS bigint
AS $$
DECLARE
  _category varchar;
  _category_name_hash bigint;
BEGIN
  _category := category(acquire_lock.stream_name);
  _category_name_hash := hash_64(_category);
  PERFORM pg_advisory_xact_lock(_category_name_hash);

  IF current_setting('debug_write', true) = 'on' OR current_setting('debug', true) = 'on' THEN
    RAISE NOTICE '» acquire_lock';
    RAISE NOTICE 'stream_name: %', acquire_lock.stream_name;
    RAISE NOTICE '_category: %', _category;
    RAISE NOTICE '_category_name_hash: %', _category_name_hash;
  END IF;

  RETURN _category_name_hash;
END;
$$ LANGUAGE plpgsql
VOLATILE;

CREATE OR REPLACE FUNCTION category(
  stream_name varchar
)
RETURNS varchar
AS $$
BEGIN
  RETURN SPLIT_PART(category.stream_name, '-', 1);
END;
$$ LANGUAGE plpgsql
IMMUTABLE;


CREATE OR REPLACE FUNCTION stream_version(
  stream_name varchar
)
RETURNS bigint
AS $$
DECLARE
  _stream_version bigint;
BEGIN
  SELECT
    max(position) into _stream_version
  FROM
    message_stream
  WHERE
    message_stream.stream_name = stream_version.stream_name;

  RETURN _stream_version;
END;
$$ LANGUAGE plpgsql
VOLATILE;



CREATE OR REPLACE FUNCTION write_message(
  id varchar,
  streams stream[],
  "type" varchar,
  data jsonb,
  metadata jsonb DEFAULT NULL
)
RETURNS bigint[]
AS $$
DECLARE
  _message_id uuid;
  _one_stream stream;
  _stream_name varchar;
  _stream_current_version bigint;
  _next_position bigint;
  _result bigint[];
BEGIN
  _result := ARRAY[]::stream[];
  _message_id = uuid(write_message.id);
  INSERT INTO messages (
    id,
    type,
    data,
    metadata
  )
  VALUES (
    _message_id,
    write_message.type,
    write_message.data,
    write_message.metadata
  );

  FOREACH _one_stream IN ARRAY write_message.streams LOOP
    _stream_name := _one_stream.stream_name;
    
    PERFORM acquire_lock(_stream_name);

    _stream_current_version := stream_version(_stream_name);
    IF _stream_current_version IS NULL THEN
      _stream_current_version := -1;
    END IF;

    IF _one_stream.expected_version IS NOT NULL THEN
      IF _one_stream.expected_version != _stream_current_version THEN
        RAISE EXCEPTION
          'Wrong expected version: % (Stream: %, Stream Version: %)',
          _one_stream.expected_version,
          _one_stream.stream_name,
          _stream_current_version;
      END IF;
    END IF;

    _next_position := _stream_current_version + 1;

    INSERT INTO message_stream (
      message_id,
      stream_name,
      position
    )
    VALUES (
      _message_id,
      _stream_name,
      _next_position
    );

    _result := _result || _next_position;

    IF current_setting('debug_write', true) = 'on' OR current_setting('debug', true) = 'on' THEN
      RAISE NOTICE '» write_message';
      RAISE NOTICE 'id ($1): %', write_message.id;
      RAISE NOTICE 'stream_name ($2): %', write_message.stream_name;
      RAISE NOTICE 'type ($3): %', write_message.type;
      RAISE NOTICE 'data ($4): %', write_message.data;
      RAISE NOTICE 'metadata ($5): %', write_message.metadata;
      RAISE NOTICE 'expected_version ($6): %', write_message.expected_version;
      RAISE NOTICE '_stream_version: %', _stream_version;
      RAISE NOTICE '_next_position: %', _next_position;
    END IF;
  END LOOP;

  RETURN _result;
END;
$$ LANGUAGE plpgsql
VOLATILE;