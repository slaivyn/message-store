import { v4 as uuidv4 } from "uuid";
import { PrismaClient } from "prisma-client";
import winston from "winston";
import { WriteFunction } from "./types";
import VersionConflictError from "./version-conflict-error";

const versionConflictErrorRegex =
  /^Wrong.*Stream: (\w+), Stream Version: (\d+)\)/;

export default function createWrite({
  prisma,
  logger,
}: {
  prisma: PrismaClient;
  logger: winston.Logger;
}): WriteFunction {
  try {
    return function write(stream, message) {
      if (!message.type) {
        throw new Error("Messages must have a type");
      }
      type StreamAndVersion = [string, bigint | null];
      const streams: StreamAndVersion[] =
        typeof stream === "string"
          ? [[stream, null]]
          : !Array.isArray(stream)
          ? [[stream.name, stream.expectedVersion || null]]
          : stream.map((s) =>
              typeof s === "string"
                ? [s, null]
                : [s.name, s.expectedVersion || null]
            );

      const uniqueStreams = streams.reduce((acc, [name, version]) => {
        const idx = acc.findIndex((s) => s[0] === name);
        const existing = acc[idx];
        if (!existing) return [...acc, [name, version]] as StreamAndVersion[];
        if (version && existing[1] && version < existing[1])
          return [
            ...acc.slice(0, idx),
            [name, version],
            ...acc.slice(idx + 1),
          ] as StreamAndVersion[];
        return acc;
      }, [] as StreamAndVersion[]);

      const id = message.id || uuidv4();
      const data = message.data || {};
      const metadata = message.metadata || {};
      const writeFunctionSql = `SELECT write_message(
      '${id}'::varchar,
      ARRAY[${uniqueStreams.map((s) => `ROW('${s[0]}', ${s[1]})`)}]::Stream[],
      '${message.type}'::varchar,
      '${JSON.stringify(data)}'::jsonb,
      '${JSON.stringify(metadata)}'::jsonb
    )`;
      console.log(writeFunctionSql, uniqueStreams);
      return prisma
        .$queryRaw(writeFunctionSql)
        .catch((err: { message: string; stack: any }) => {
          console.log("err write", err);
          const errorMatch = err.message.match(versionConflictErrorRegex);

          if (errorMatch === null || uniqueStreams.every((s) => s[1] == null)) {
            throw err;
          }

          const streamName = errorMatch[1];
          const actualVersion = parseInt(errorMatch[2], 10);
          const expectedVersion = uniqueStreams.find(
            (s) => s[0] === streamName
          )?.[1];

          const versionConflictError = new VersionConflictError(
            streamName,
            actualVersion,
            expectedVersion
          );
          versionConflictError.stack = err.stack;

          throw versionConflictError;
        });
    };
  } catch (err) {
    logger.error(err);
    throw err;
  }
}

module.exports = createWrite;
